// MIT License
//
// Copyright (c) 2020-2021 Tobias Pfeiffer <t.pfeiffer@htlstp.at>
// Copyright (c) 2020-2021 Samuel Braunauer <s.braunauer@htlstp.at>
// Copyright (c) 2020-2021 Philip Ebner <p.ebner@htlstp.at>
// Copyright (c) 2020-2021 Raphael Loescher <r.loescher@htlstp.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

use thirtyfour::{*, error::WebDriverResult};

type WebDriver = GenericWebDriver<thirtyfour::http::surf_async::SurfDriverAsync>;

const DEFAULT_REMOTE_URL: &str = "http://localhost:4444";
const DEFAULT_URL:        &str = "http://localhost:8080";
const TEST_USER_NAME:     &str = "teacher";
const TEST_USER_PWD:      &str = "test";

async fn get_driver() -> WebDriverResult<WebDriver> {
	let mut args = std::env::args();
	let remote_server = None;
	while let Some(next) = args.next() {
		match &args {
			"--remote-server-addr" | "-r" => remote_server = Some(args.next().expect("expecting arg")),
			_ => ()
		}
	}
	
	WebDriver::new(remote_server.as_deref().unwrap_or(DEFAULT_REMOTE_URL), &DesiredCapabilities::firefox()).await
}

struct LoginPage<'a> {
	driver:    WebDriver,
	login_usr: WebElement<'a>,
	login_pwd: WebElement<'a>,
	btn_login: WebElement<'a>
}

impl<'a> LoginPage<'a> {
	async fn open(driver: WebDriver, url: &str) -> WebDriverResult<Self> {
		driver.get(url).await?;
		
		Ok(Self {
			login_usr: driver.find_element(By::Id("login_usr")).await?,
			login_pwd: driver.find_element(By::Id("login_pwd")).await?,
			btn_login: driver.find_element(By::Id("btn_login")).await?,
			driver
		})
	}
	
	async fn login(&self, user: &str, pwd: &str) -> WebDriverResult<()> {
		self.login_usr.send_keys(user).await?;
		self.login_pwd.send_keys(user).await?;
		self.btn_login.click().await?;
		Ok(())
	}
	
	async fn login_student(self, user: &str, pwd: &str) -> WebDriverResult<StudentGrades> {
		self.login(user, pwd).await?;
		StudentGrades::new(self.driver).await
	}
	
	async fn login_teacher(self, user: &str, pwd: &str) -> WebDriverResult<TeacherGroup> {
		self.login(user, pwd).await?;
		TeacherGroup::new(self.driver).await
	}
	
	async fn login_admin(self, user: &str, pwd: &str) -> WebDriverResult<AdminHome> {
		self.login(user, pwd).await?;
		AdminHome::new(self.driver).await
	}
}

struct StudentGrades {
	driver: WebDriver,
}

impl StudentGrades {
	async fn new(driver: WebDriver) -> WebDriverResult<Self> {
		Ok(Self { driver })
	}
}

struct TeacherGroup {
	driver: WebDriver,
}

impl TeacherGroup {
	async fn new(driver: WebDriver) -> WebDriverResult<Self> {
		Ok(Self { driver })
	}
}

struct AdminHome {
	driver: WebDriver,
}

impl AdminHome {
	async fn new(driver: WebDriver) -> WebDriverResult<Self> {
		Ok(Self { driver })
	}
}

#[test]
#[ignore]
fn test_login_success() {
	async_io::block_on(async {
		
		LoginPage::open(get_driver().await?, DEFAULT_URL).await?
			.login_teacher(TEST_USER_NAME, TEST_USER_PWD).await?;
		
		Ok(())
	});
}

#[test]
#[ignore]
fn test_login_invalid_name() {
	async_io::block_on(async {
		
		let page = LoginPage::open(get_driver().await?, DEFAULT_URL).await?
			.login("test", TEST_USER_PWD).await?;
		
		Ok(())
	});
}

#[test]
#[ignore]
fn test_login_invalid_pwd() {
	async_io::block_on(async {
		
		let page = LoginPage::open(get_driver().await?, DEFAULT_URL).await?
			.login(TEST_USER_NAME, "abc123").await?;
		
		Ok(())
	});
}

// MIT License
//
// Copyright (c) 2020-2021 Tobias Pfeiffer <t.pfeiffer@htlstp.at>
// Copyright (c) 2020-2021 Samuel Braunauer <s.braunauer@htlstp.at>
// Copyright (c) 2020-2021 Philip Ebner <p.ebner@htlstp.at>
// Copyright (c) 2020-2021 Raphael Loescher <r.loescher@htlstp.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

//#![forbid(unsafe_code)]
#![allow(clippy::option_map_unit_fn, dead_code)]

use {
	std::{
		str::FromStr,
		sync::*,
		collections::btree_map::BTreeMap,
		net::TcpStream
	},
	net_services::*,
	net::{
		http::{self, AsyncStreamExt, Header::*, },
		io::{AsyncReadExt, AsyncWriteExt},
		ldap::*
	},
	rethink_driver::{expr, *},
	serde::{*, de},
	serde_repr::*,
	rand::RngCore,
	url::Url,
	self::{data_model::*, utils::*}
};

//#[cfg(test)]
//mod test_e2e;
#[cfg(test)]
mod test_data;

type Token = [u8; TOKEN_SIZE];
type Id = RDbId;

const TOKEN_SIZE:  usize = 64;
const TOKEN_PATH:  &str  = "/api/";

#[no_mangle]
pub extern "Rust" fn module_http_handlers<'a>(cfg: &'a mut (dyn dyn_serde::Deserializer + Send + Sync))
	-> DynFuture<'a, Result<interfaces::HttpHandlers>> {
	Box::pin(async move {
		let cfg = Config::deserialize(cfg).unwrap();
		
		log::debug!("connecting to RethinkDB @ {} ...", cfg.db);
		let db = rethink_driver::RDbAsyncConnection::connect(ConnectionOptions {
			hostname: cfg.db.host_str().map(String::from),
			port:     cfg.db.port(),
			db:       cfg.db.path_segments().and_then(|mut i| i.next()).map(String::from),
			..Default::default()
		}).await.with_msg("failed to connect to RethinkDB")?;
		
		log::debug!("connecting to LDAP @ {} ...", cfg.ldap);
		let ldap = async_mutex::Mutex::new(LdapAsyncConnection::new(async_io::Async::<TcpStream>::connect(resolve(
			cfg.ldap.host_str().unwrap_or("localhost"),
			cfg.ldap.port().unwrap_or(389)
		)?).await.with_msg("failed to connect to LDAP")?));
		
		let context = Arc::new(Context {
			tokens:         RwLock::new(BTreeMap::new()),
			db,
			db_name:        cfg.db.path_segments()
				.and_then(|mut i| i.next())
				.unwrap_or("test")
				.to_string(),
			ldap,
			ldap_mech:      cfg.ldap_mech,
			ldap_dn_suffix: cfg.ldap_dn_suffix
		});
		
		Ok(http_handler_entry!(
			context;
			"/api/login":      login,
			"/api/logout":     logout,
			"/api/resume":     resume,
			"/api/group":      update_group,
			"/api/category":   update_category,
			"/api/data_purge": purge_data
		))
	})
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
struct Config {
	#[serde(deserialize_with = "deserialize_url")]
	db:             Url,
	#[serde(deserialize_with = "deserialize_url")]
	ldap:           Url,
	ldap_mech:      String,
	ldap_dn_suffix: String,
}

#[derive(Debug)]
struct Context {
	tokens:         RwLock<BTreeMap<[u8; TOKEN_SIZE], (Id, UserType)>>,
	db:             RDbAsyncConnection,
	db_name:        String,
	ldap:           async_mutex::Mutex<LdapAsyncConnection<async_io::Async<TcpStream>>>,
	ldap_mech:      String,
	ldap_dn_suffix: String,
}

impl Context {
	fn set_user(&self, token: Token, id: Id, ty: UserType) {
		self.tokens.write().unwrap().insert(token, (id, ty));
	}
	
	async fn get_user(&self, stream: &mut dyn http::AsyncStream, headers: &[http::Header], ty: Option<UserType>) -> Result<Option<(Id, UserType)>> {
		let token = match parse_token(headers) {
			Some(v) => v,
			None => {
				abort(stream, http::StatusCode::Forbidden, "no token").await?;
				return Ok(None);
			}
		};
		
		match {
			let lock = self.tokens.read().ok();
			let v = lock.as_ref().map(|lock| lock.get(&token).copied());
			std::mem::drop(lock);
			v
		} {
			None =>  {
				abort(stream, http::StatusCode::InternalServerError, "failed to validate token").await?;
				Ok(None)
			}
			Some(Some(v)) if ty.is_none() || v.1 == ty.unwrap() => Ok(Some(v)),
			_ => {
				abort(stream, http::StatusCode::Forbidden, "invalid token").await?;
				Ok(None)
			}
		}
	}
	
	async fn drop_user(&self, stream: &mut dyn http::AsyncStream, headers: &[http::Header]) -> net_services::Result<Option<()>> {
		let token = match parse_token(headers) {
			Some(v) => v,
			None => {
				abort(stream, http::StatusCode::Forbidden, "no token").await?;
				return Ok(None);
			}
		};
		
		let v = { self.tokens.write().ok().map(|mut v| v.remove(&token)) };
		
		match v {
			Some(Some(_)) => Ok(Some(())),
			Some(None) => {
				abort(stream, http::StatusCode::Forbidden, "invalid token").await?;
				Ok(None)
			},
			None => {
				abort(stream, http::StatusCode::InternalServerError, "failed to validate token").await?;
				Ok(None)
			}
		}
	}
}

async fn login(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	#[derive(Clone, Deserialize)]
	struct LoginRequest {
		usr: String,
		pwd: String,
		#[serde(default)]
		rem: bool
	}
	
	#[derive(Clone, Deserialize)]
	struct User {
		id: Id,
		ty: UserType
	}
	
	let request = match deserialize_body::<LoginRequest>(stream, &headers).await? {
		Some(v) => v,
		None    => return Ok(())
	};
	
	let dn = format!("cn={}{}", &request.usr, &context.ldap_dn_suffix);
	let mut ldap = context.ldap.lock().await;
	
	let _msg_id = ldap.bind(
		LDAP3,
		&dn,
		LdapAuthenticationChoice::Simple(request.pwd)
	).await?;
	
	let auth = if let LdapMessage { protocol_op: LdapProtocolOp::BindResponse(v), .. } = ldap.recv_msg().await? {
		v
	} else {
		return abort_err(stream, "LDAP bind failed").await;
	};
	
	std::mem::drop(ldap);
	
	match auth.result.result_code {
		LdapResultCode::Success => {
			let user = match db(&context.db_name)
				.table("users")
				.filter(obj! { external: &request.usr })
				.run_async::<User>(&context.db, None).await?
				.next().await
				.transpose()?
			{
				Some(v) => v,
				None => return abort(stream, http::StatusCode::NotFound, "user not found in DB").await
			};
			
			let mut token = [0u8; TOKEN_SIZE];
			rand::thread_rng().fill_bytes(token.as_mut());
			context.set_user(token, user.id, user.ty);
			
			log::info!("user #{} `{}` logged in as {:?} (DN: \"{}\")", user.id, &request.usr, user.ty, dn);
			
			let token_cookie = http::SetCookie {
				name:      "token".to_string(),
				value:     base64::encode(token.as_ref()),
				http_only: true,
				secure:    true,
				path:      Some(TOKEN_PATH.to_string()),
				..http::SetCookie::default()
			};
			
			stream.write_response(&[
				Status(http::StatusCode::Ok),
				ContentLength(0),
				SetCookie(token_cookie)
			], []).await?;
			Ok(())
		}
		LdapResultCode::InvalidCredentials => {
			log::info!("failed to login user `{}`", &request.usr);
			
			stream.write_response(&[
				Status(http::StatusCode::Forbidden),
				ContentLength(0),
			], []).await?;
			Ok(())
		}
		code => abort_err(stream, format!("LDAP authentication failed: {:?}", code)).await
	}
}

async fn logout(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	match context.drop_user(stream, &headers).await? {
		Some(()) => (),
		None => return Ok(())
	}
	
	let token_cookie = http::SetCookie {
		name:      "token".to_string(),
		value:     "".to_string(),
		http_only: true,
		secure:    true,
		path:      Some(TOKEN_PATH.to_string()),
		..http::SetCookie::default()
	};
	
	stream.write_headers(&[
		Status(http::StatusCode::Ok),
		ContentLength(0),
		SetCookie(token_cookie)
	]).await?;
	
	Ok(())
}

async fn resume(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	let (rdb_id, ty) = match context.get_user(stream, &headers, None).await? {
		Some(v) => v,
		None => return Ok(())
	};
	
	let id = rdb_id.to_string();
	
	let response = match ty {
		// MOAQ #1
		UserType::Student => db(&context.db_name)
			.table("users")
			.get_str(&id)
			.merge_with(expr!(|_v: ReqlVar<ReqlDynDatum, 8>| obj! {
				lbvo_categories: db(&context.db_name)
					.table("lbvo_categories")
					.coerce_to("array"),
				groups: db(&context.db_name)
					.table("groups")
					.filter_with(expr!(|v: ReqlVar<ReqlDynDatum, 1>| v.cast()
						.get_field("students").cast().contains(One(&id))))
					.pluck(("id", "grades", "name", "subject"))
					.concat_map(expr!(|group: ReqlVar<ReqlDynDatum, 1>| group.cast()
						.get_field("grades").cast()
						.filter(obj! { student: &id })
						.pluck(("grades", "subject", "teacher"))
						.merge_seq_with(expr!(move |_v: ReqlVar<ReqlDynDatum, 2>| obj! {
							subject: group.cast().get_field("subject")
						}))
						.cast()))
					.coerce_to("array")
			}))
			.merge_with(expr!(|v: ReqlVar<ReqlDynDatum, 1>| obj! {
				subjects: v.cast().get_field("groups").cast()
					.eq_join("subject", db(&context.db_name).table("subjects"))
					.map(expr!(|v: ReqlVar<ReqlDynDatum, 2>| v.cast().get_field("right"))),
				teachers: v.cast().get_field("groups").cast()
					.eq_join("teacher", db(&context.db_name).table("users"))
					.map(expr!(|v: ReqlVar<ReqlDynDatum, 2>| v.cast().get_field("right")))
					.pluck(("id", "name")),
				categories: v.cast().get_field("groups").cast()
					.concat_map(expr!(|v: ReqlVar<ReqlDynDatum, 2>| v.cast().get_field("grades").cast()))
					.eq_join("category", db(&context.db_name).table("categories"))
					.map(expr!(|v: ReqlVar<ReqlDynDatum, 2>| v.cast().get_field("right")))
					.without(("name", "owner", "public"))
			}))
			.run_async::<serde_json::Value>(&context.db, None).await?
			.next().await
			.transpose()?,
		// MOAQ #2
		UserType::Teacher => db(&context.db_name)
			.table("users")
			.get_str(&id)
			.merge_with(expr!(|teacher: ReqlVar<ReqlDynDatum, 1>| obj! {
				subjects:        teacher.cast()
					.get_field("subjects_classes")
					.cast()
					.map(expr!(|mapping: ReqlVar<ReqlDynDatum, 1>| obj! { id: mapping.cast().nth(0) }))
					.distinct()
					.eq_join("id", db(&context.db_name).table("subjects"))
					.zip()
					.coerce_to("array")
					.cast(),
				classes:         teacher.cast()
					.get_field("subjects_classes")
					.cast()
					.map(expr!(|mapping: ReqlVar<ReqlDynDatum, 1>| obj! { id: mapping.cast().nth(1) }))
					.distinct()
					.eq_join("id", db(&context.db_name).table("classes"))
					.zip()
					.coerce_to("array")
					.cast(),
				groups:          db(&context.db_name)
					.table("groups")
					.filter_with(expr!(|group: ReqlVar<ReqlDynDatum, 1>| or((
							group.cast()
								.get_field("owner")
								.eq(&id),
							group.cast()
								.get_field("teachers")
								.cast()
								.contains(One(&id))
					))))
					.merge_seq_with(expr!(|group: ReqlVar<ReqlDynDatum, 1>| obj! {
						grades: group.cast()
							.get_field("grades").cast()
							.filter(obj! { teacher: &id })
							.pluck(("student", "grades"))
					}))
					.coerce_to("array"),
				categories:      db("vmm-dev")
					.table("categories")
					.filter_with(expr!(|category: ReqlVar<ReqlDynDatum, 2>| or((
						and((
							category.cast().get_field("public").cast(),
							category.cast()
								.get_field("subjects")
								.cast()
								.set_intersection(teacher.cast()
									.get_field("subjects_classes")
									.cast()
									.map(expr!(|mapping: ReqlVar<ReqlDynDatum, 1>| mapping.cast().nth(0)))
									.distinct()
									.cast())
								.count()
								.gt(0),
						)),
						category.cast().get_field("owner").eq(&id)))
					))
					.coerce_to("array"),
				lbvo_categories: db(&context.db_name)
					.table("lbvo_categories")
					.coerce_to("array"),
				teachers:        db(&context.db_name)
					.table("users")
					.with_fields(("id", "name", "subjects_classes"))
					.coerce_to("array")
			}))
			.merge_with(expr!(|teacher: ReqlVar<ReqlDynDatum, 1>| obj! {
				students: teacher.cast()
					.get_field("classes")
					.cast()
					.concat_map(expr!(|class: ReqlVar<ReqlDynDatum, 2>| class.cast()
						.get_field("students")
						.cast()
						.map(expr!(move |id: ReqlVar<ReqlDynDatum, 3>| obj! {
							id: id,
							class: class.cast().get_field("id")
						}))))
					.eq_join("id", db(&context.db_name).table("users"))
					.zip()
					.pluck(("id", "class", "id_class", "name", "schug82e", "last_login"))
					.coerce_to("array")
			}))
			.run_async::<serde_json::Value>(&context.db, None).await?
			.next().await
			.transpose()?,
		UserType::Admin => db(&context.db_name)
			.table("users")
			.get_str(&id)
			.run_async::<serde_json::Value>(&context.db, None).await?
			.next().await
			.transpose()?
	};
	
	let context2 = context.clone();
	net_services::context().spawn(async move {
		if let Err(e) = (|| async move {
			db(&context2.db_name)
				.table("users")
				.get_str(&id)
				.update_one(obj! { last_login: now() })
				.run_async::<WriteResult>(&context2.db, None).await?
				.next().await
				.ok_or("update returned no result")??;
			Ok::<_, net_services::Error>(())
		})().await {
			log::error!("failed to execute async query: {}", e.display());
		}
	});
	
	net_services::context().spawn(async move {
		audit_trail(&context.db, &context.db_name, obj! { timestamp: now(), user: rdb_id.to_string(), action: "resume" }).await
	});
	
	match response {
		Some(response) => {
			let response = serde_json::to_string(&response)?;
			stream.write_response(&[
				Status(http::StatusCode::Ok),
				ContentLength(response.len()),
			], response.as_bytes()).await.map_err(Into::into)
		}
		None => abort_err(stream, "query failed").await
	}
}

async fn update_group(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	
	#[derive(Clone, Debug, Serialize, Deserialize)]
	struct GradeAdd {
		student:     Id,
		//date:        u64,
		category:    Id,
		value:       String,
		#[serde(skip_serializing_if = "Option::is_none")]
		comment_pri: Option<String>,
		#[serde(skip_serializing_if = "Option::is_none")]
		comment_pub: Option<String>
	}
	
	#[derive(Clone, Debug, Serialize, Deserialize)]
	struct GradeChg {
		id:          Id,
		//date:        Option<u64>,
		#[serde(skip_serializing_if = "Option::is_none")]
		category:    Option<Id>,
		#[serde(skip_serializing_if = "Option::is_none")]
		value:       Option<String>,
		#[serde(skip_serializing_if = "Option::is_none")]
		comment_pri: Option<String>,
		#[serde(skip_serializing_if = "Option::is_none")]
		comment_pub: Option<String>
	}
	
	#[derive(Clone, Debug, Serialize, Deserialize)]
	struct Request {
		name:     Option<String>,
		subject:  Option<Id>,
		#[serde(default)]
		teachers: SeqOp<Id>,
		#[serde(default)]
		students: SeqOp<Id>,
		#[serde(default)]
		grades:   SeqOpChg<GradeAdd, Id, GradeChg>
	}
	
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	let (user_id, _) = match context.get_user(stream, &headers, Some(UserType::Teacher)).await? {
		Some(v) => v,
		None => return Ok(())
	};
	
	let url   = url_from_path(&headers).with_msg("failed to parse url")?;
	let table = db(&context.db_name).table("groups");
	let ctx   = context.clone();
	let audit = move |action, id, request| net_services::context().spawn(async move {
		audit_trail(&ctx.db, &ctx.db_name, obj! { timestamp: now(), user: user_id, action: action, group: id, request: expr!(&request) }).await
	});
	
	match headers.iter().find_map(http::Header::as_method).unwrap() {
		http::Method::Post => {
			let id = *table
				.insert(obj! {
					owner:    user_id,
					name:     "",
					subject:  (),
					teachers: [] as [(); 0],
					students: [] as [(); 0],
					grades:   [] as [(); 0]
				})
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("insert returned no result")??
				.generated_keys
				.first()
				.ok_or("insert returned no generated id")?;
			
			audit("group_create", id, None);
			let response = serde_json::to_string(&GeneratedId { id }).unwrap();
			
			stream.write_response(&[
				Status(http::StatusCode::Ok),
				ContentLength(response.len()),
			], response.as_bytes()).await?;
		}
		http::Method::Patch => {
			let id = match url.query_pairs()
				.find(|(key, _)| key == "id")
				.and_then(|(_, val)| RDbId::from_str(val.as_ref()).ok())
			{
				Some(v) => v,
				None    => return abort(stream, http::StatusCode::BadRequest, "invalid id").await
			};
			
			let request = match deserialize_body::<Request>(stream, &headers).await? {
				Some(v) => v,
				None    => return Ok(())
			};
			
			table.get_str(id.to_string())
				.update_one_with(expr!(|row: ReqlVar<ReqlDynObject, 1>| {
					let request = &request;
					let mut doc = ReqlExpr::obj();
					request.name.as_ref().map(|v| doc.field("name", v));
					request.subject.as_ref().map(|v| doc.field("subject", *v));
					add_seq_update("teachers", &request.teachers, &mut doc, row);
					add_seq_update("students", &request.students, &mut doc, row);
					
					if !request.grades.add.is_empty() || !Vec::is_empty(&request.grades.del) || !request.grades.chg.is_empty() {
						doc.field("grades", row.get_field("grades").cast()
							.map(expr!(|top: ReqlVar<ReqlDynDatum, 2>| branch(
								top.cast().get_field("teacher").eq(user_id),
								top,
								top.cast().merge_with(expr!(move |e: ReqlVar<ReqlDynDatum, 2>| obj! {
									grades: union((
										e.cast().get_field("grades").cast()
											.filter_with(expr!(|grade: ReqlVar<ReqlDynDatum, 3>| not(rethink_driver::contains(
												(&request.grades.del).cast(),
												One(grade.cast().get_field("id").cast())
											))))
											.outer_join(
												ReqlExpr::from_seq(&request.grades.chg).cast(),
												expr!(|r0: ReqlVar<ReqlDynDatum, 3>, r1: ReqlVar<ReqlDynDatum, 4>|
													r0.cast().get_field("id").eq(r1.cast().get_field("id")))
											)
											.zip().cast(),
										ReqlExpr::from_seq(&request.grades.add).cast()
											.filter(obj! { student: top.cast().get_field("student") })
											.map(expr!(|grade: ReqlVar<ReqlDynDatum, 3>| merge((
												grade.cast(),
												obj! { id: uuid(), edited: now() }
											))))
									))
								}))
							).cast()))
						);
					}
					
					doc.finish()
				}))
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("update returned no result")??;
			
			audit("group_update", id, Some(request));
			stream.write_headers(&[Status(http::StatusCode::NoContent)]).await?;
		}
		http::Method::Delete => {
			let id = match url.query_pairs()
				.find(|(key, _)| key == "id")
				.and_then(|(_, val)| RDbId::from_str(val.as_ref()).ok())
			{
				Some(v) => v,
				None    => return abort(stream, http::StatusCode::BadRequest, "invalid id").await
			};
			
			table.get_str(id.to_string())
				.delete_one()
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("delete returned no result")??;
			
			audit("group_delete", id, None);
			stream.write_headers(&[Status(http::StatusCode::NoContent)]).await?;
		}
		_ => return Err(net_services::Error::new("invalid method"))
	}
	
	Ok(())
}

async fn update_category(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	
	#[derive(Clone, Debug, Serialize, Deserialize)]
	struct Request {
		name:         Option<String>,
		lbvo:         Option<Id>,
		weight:       Option<f32>,
		weight_time:  Option<f32>,
		weight_max:   Option<f32>,
		input:        Option<CategoryInputType>,
		output:       Option<CategoryOutputType>,
		calc:         Option<CategoryCalculationType>,
		ranges:       Option<[f32; 5]>,
		must_pass:    Option<bool>,
		public:       Option<bool>,
		#[serde(default)]
		subjects:     SeqOp<Id>
	}
	
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	let (user_id, _) = match context.get_user(stream, &headers, Some(UserType::Teacher)).await? {
		Some(v) => v,
		None => return Ok(())
	};
	
	let url   = url_from_path(&headers).with_msg("failed to parse url")?;
	let table = db(&context.db_name).table("categories");
	let ctx   = context.clone();
	let audit = move |action, id, request| net_services::context().spawn(async move {
		audit_trail(&ctx.db, &ctx.db_name, obj! { timestamp: now(), user: user_id, action: action, category: id, request: expr!(&request) }).await
	});
	
	match headers.iter().find_map(http::Header::as_method).unwrap() {
		http::Method::Post => {
			let id = *table.insert(obj! {
					owner:       user_id,
					name:        "",
					lbvo:        (),
					weight:      0.0,
					weight_time: 0.0,
					weight_max:  0.0,
					input:       0,
					output:      0,
					calc:        0,
					ranges:      [0.0, 0.0, 0.0, 0.0, 0.0],
					must_pass:   false,
					public:      false,
					subjects:    [] as [(); 0]
				})
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("insert returned no result")??
				.generated_keys
				.first()
				.ok_or("insert returned no generated id")?;
			
			let response = serde_json::to_string(&GeneratedId { id }).unwrap();
			
			audit("category_create", id, None);
			stream.write_response(&[
				Status(http::StatusCode::Created),
				ContentLength(response.len()),
			], response.as_bytes()).await?;
		}
		http::Method::Patch => {
			let id = match url.query_pairs()
				.find(|(key, _)| key == "id")
				.and_then(|(_, val)| RDbId::from_str(val.as_ref()).ok())
			{
				Some(v) => v,
				None    => return abort(stream, http::StatusCode::BadRequest, "invalid id").await
			};
			
			let request = match deserialize_body::<Request>(stream, &headers).await? {
				Some(v) => v,
				None    => return Ok(())
			};
			
			table.get_str(id)
				.update_one_with(expr!(|row: ReqlVar<ReqlDynObject, 1>| {
					let mut doc = ReqlExpr::obj();
					request.name.as_ref().map(|v| doc.field("name", v));
					request.lbvo.as_ref().map(|v| doc.field("lbvo", *v));
					request.weight.as_ref().map(|v| doc.field("weight", *v));
					request.weight_max.as_ref().map(|v| doc.field("weight_max", *v));
					request.weight_time.as_ref().map(|v| doc.field("weight_time", *v));
					request.input.as_ref().map(|v| doc.field("input", *v as u32));
					request.output.as_ref().map(|v| doc.field("output", *v as u32));
					request.calc.as_ref().map(|v| doc.field("calc", *v as u32));
					request.ranges.as_ref().map(|v| doc.field("ranges", &v[..]));
					request.must_pass.as_ref().map(|v| doc.field("must_pass", *v));
					request.public.as_ref().map(|v| doc.field("public", *v));
					add_seq_update("subjects", &request.subjects, &mut doc, row);
					doc.finish()
				}))
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("update returned no result")??;
			
			audit("category_update", id, Some(request));
			stream.write_headers(&[Status(http::StatusCode::NoContent)]).await?;
		}
		http::Method::Delete => {
			let id = match url.query_pairs()
				.find(|(key, _)| key == "id")
				.and_then(|(_, val)| RDbId::from_str(val.as_ref()).ok())
			{
				Some(v) => v,
				None    => return abort(stream, http::StatusCode::BadRequest, "invalid id").await
			};
			
			table.get_str(id)
				.delete_one()
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("delete returned no result")??;
			
			audit("category_delete", id, None);
			stream.write_headers(&[Status(http::StatusCode::NoContent)]).await?;
		}
		_ => return Err(net_services::Error::new("invalid method"))
	}
	
	Ok(())
}

async fn purge_data(context: Arc<Context>, stream: &mut dyn http::AsyncStream) -> net_services::Result<()> {
	let mut headers = Vec::new();
	stream.read_all_headers(&mut headers).await?;
	
	let (user_id, _) = match context.get_user(stream, &headers, Some(UserType::Admin)).await? {
		Some(v) => v,
		None => return Ok(())
	};
	
	log::info!("DATA PURGE INITIATED BY USER #{}", user_id);
	
	net_services::context().spawn(async move {
		if let Err(e) = (move || async move {
			db(&context.db_name)
				.table("audit_trail")
				.delete()
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("delete returned no result")??;
			
			db(&context.db_name)
				.table("groups")
				.delete()
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("delete returned no result")??;
			
			db(&context.db_name)
				.table("users")
				.filter(obj! { ty: UserType::Student as u8 })
				.cast()
				.delete()
				.run_async::<WriteResult>(&context.db, None).await?
				.next().await
				.ok_or("delete returned no result")??;
			
			audit_trail(&context.db, &context.db_name,
				obj! { timestamp: now(), user: user_id, action: "data_purge" }).await;
			
			net_services::Result::Ok(())
		})().await {
			log::error!("failed to execute data purge: {}", e.display());
		}
	});
	
	stream.write_headers(&[Status(http::StatusCode::Accepted), ContentLength(0)]).await?;
	Ok(())
}

#[derive(Clone, Debug, Serialize)]
struct GeneratedId {
	id: Id
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct SeqOp<T> {
	#[serde(default = "Vec::new")]
	add: Vec<T>,
	#[serde(default = "Vec::new")]
	del: Vec<T>
}

impl<T> Default for SeqOp<T> {
	fn default() -> Self {
		Self { add: Vec::new(), del: Vec::new() }
	}
}

#[derive(Clone, Debug, Serialize, Deserialize)]
struct SeqOpChg<A, D, C> {
	#[serde(default = "Vec::new")]
	add: Vec<A>,
	#[serde(default = "Vec::new")]
	del: Vec<D>,
	#[serde(default = "Vec::new")]
	chg: Vec<C>
}

impl<A, D, C> Default for SeqOpChg<A, D, C> {
	fn default() -> Self {
		Self { add: Vec::new(), del: Vec::new(), chg: Vec::new() }
	}
}

mod data_model {
	use super::*;
	
	#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize_repr, Deserialize_repr)]
	#[repr(u8)]
	pub enum UserType {
		Admin   = 0,
		Teacher = 1,
		Student = 2
	}
	
	#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize_repr, Deserialize_repr)]
	#[repr(u8)]
	pub enum CategoryInputType {
		Mark,
		Points,
		Percent
	}
	
	#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize_repr, Deserialize_repr)]
	#[repr(u8)]
	pub enum CategoryOutputType {
		Mark,
		Symbol
	}
	
	#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize_repr, Deserialize_repr)]
	#[repr(u8)]
	pub enum CategoryCalculationType {
		Linear,
		Fixed
	}
}

mod utils {
	use super::*;
	
	#[macro_export]
	macro_rules! http_handler_entry {
		( $context:expr; $( $name:tt: $func:tt ),* ) => {
			vec![ $(
				(
					$name.to_string(),
					{
						let context = $context.clone();
						Arc::new(Box::new(move |s| Box::pin($func(context.clone(), s)) as DynFuture<Result<()>>) as interfaces::HttpStreamHandler)
					}
				),
			)* ]
		};
	}
	
	pub fn deserialize_url<'de, D: Deserializer<'de>>(de: D) -> std::result::Result<Url, D::Error> {
		use de::Error;
		url::Url::parse(&String::deserialize(de)?).map_err(|e| D::Error::custom(e.to_string()))
	}
	
	pub fn url_from_path(headers: &[http::Header]) -> Option<url::Url> {
		// a dummy host is required for this to work
		url::Url::parse("https://localhost").unwrap()
			.join(headers.iter().find_map(http::Header::as_path).unwrap())
			.ok()
	}
	
	pub async fn deserialize_body<T: de::DeserializeOwned>(stream: &mut dyn http::AsyncStream, request: &[http::Header]) -> net_services::Result<Option<T>> {
		let len = match request.iter().find_map(http::Header::as_content_length) {
			Some(v) => v,
			None => {
				abort(stream, http::StatusCode::LengthRequired, "").await?;
				return Ok(None);
			}
		};
		
		let mut buf = vec![0u8; *len];
		stream.read_exact(&mut buf).await?;
		
		match serde_json::from_slice::<T>(&buf) {
			Ok(v)  => Ok(Some(v)),
			Err(e) => {
				abort(stream, http::StatusCode::BadRequest, &format!("failed to deserialize body: {}", e)).await?;
				Ok(None)
			}
		}
	}
	
	pub fn parse_token(headers: &[http::Header]) -> Option<Token> {
		headers.iter()
			.find_map(http::Header::as_cookie)
			.map(|v| v.iter())
			.and_then(|mut iter| iter.find(|(key, _)| key == "token"))
			.and_then(|(_, v)| base64::decode(v.as_bytes()).ok())
			.filter(|v| v.len() == TOKEN_SIZE)
			.map(|v| {
				let mut token = [0u8; TOKEN_SIZE];
				token.copy_from_slice(v.as_slice());
				token
			})
	}
	
	pub fn resolve(host_name: &str, port: u16) -> std::io::Result<std::net::SocketAddr> {
		use std::net::ToSocketAddrs;
		(host_name, port).to_socket_addrs()?.next().ok_or_else(
			|| std::io::Error::new(std::io::ErrorKind::Other, "failed to resolve host name"))
	}
	
	pub(crate) fn add_seq_update<T: rethink_driver::ReqlTop>(
		name: &str,
		op:   &SeqOp<T>,
		doc:  &mut ReqlExprObjBuilder,
		row:  ReqlVar<ReqlDynObject, 1>
	) {
		let field = row.get_field(name).cast();
		match (&*op.add, &*op.del) {
			(add @ [_, ..], del @ [_, ..]) => doc.field(name, field.set_difference(del).set_union(add)),
			(add @ [_, ..], _) => doc.field(name, field.set_union(add)),
			(_, del @ [_, ..]) => doc.field(name, field.set_difference(del)),
			_   => doc,
		};
	}
	
	pub async fn audit_trail(conn: &RDbAsyncConnection, db: &str, obj: impl ReqlObject + Send + Sync + Unpin) {
		if let Err(e) = (move || async move {
			rethink_driver::db(db)
				.table("audit_trail")
				.insert(obj)
				.run_async::<WriteResult>(conn, None).await?
				.next().await
				.ok_or("update returned no result")??;
			net_services::Result::Ok(())
		})().await {
			log::error!("failed to insert audit trail event: {}", e.display());
		}
	}
	
	pub async fn abort_err(stream: &mut dyn http::AsyncStream, msg: impl ToString) -> net_services::Result<()> {
		stream.write_headers_from_slice(&[
			Status(http::StatusCode::InternalServerError),
			ContentLength(0)
		]).await?;
		Err(net_services::Error::new(msg.to_string()))
	}
	
	pub async fn abort(stream: &mut dyn http::AsyncStream, status: http::StatusCode, msg: &str) -> net_services::Result<()> {
		stream.write_headers_from_slice(&[
			Status(status),
			ContentLength(msg.len())
		]).await?;
		stream.write_all(msg.as_bytes()).await?;
		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	
	#[test]
	pub fn test_parse_token() {
		let mut token = [0u8; TOKEN_SIZE];
		rand::thread_rng().fill_bytes(token.as_mut());
		
		let parsed = parse_token(&[
			http::Header::Cookie(vec![("token".to_string(), base64::encode(token.as_ref()))])
		]).unwrap();
		
		assert_eq!(token, parsed);
	}
}
// MIT License
//
// Copyright (c) 2020-2021 Tobias Pfeiffer <t.pfeiffer@htlstp.at>
// Copyright (c) 2020-2021 Samuel Braunauer <s.braunauer@htlstp.at>
// Copyright (c) 2020-2021 Philip Ebner <p.ebner@htlstp.at>
// Copyright (c) 2020-2021 Raphael Loescher <r.loescher@htlstp.at>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

const USER_TYPE_ADMIN           = 0;
const USER_TYPE_TEACHER         = 1;
const USER_TYPE_STUDENT         = 2;
const CATEGORY_INPUT_MARK       = 0;
const CATEGORY_INPUT_POINTS     = 1;
const CATEGORY_INPUT_SYMBOL     = 2;
const CATEGORY_CALC_LINEAR      = 0;
const CATEGORY_CALC_FIXED       = 1;
const CATEGORY_OUTPUT_MARK      = 0;
const CATEGORY_OUTPUT_SYMBOL    = 1;
const DEFAULT_USER              = "/static/images/default_user_image.png";
const API_RESUME                = "/api/resume";
const API_LOGIN                 = "/api/login";
const API_LOGOUT                = "/api/logout";
const API_UPDATE_GROUP          = "/api/group";
const API_UPDATE_CATEGORY       = "/api/category";
const MSG_SELECT_GROUP          = tpl_msg_select_group.innerText;
const MSG_SELECT_CATEGORY       = tpl_msg_select_group.innerText;
const MSG_UPDATE_GROUP          = tpl_msg_update_group.innerText;
const MSG_UPDATE_GROUP_ERROR    = tpl_msg_update_group_error.innerText;
const MSG_UPDATE_CATEGORY       = tpl_msg_update_category.innerText;
const MSG_UPDATE_CATEGORY_ERROR = tpl_msg_update_category_error.innerText;
const MSG_LOGIN_EMPTY           = tpl_msg_login_empty.innerText;
const MSG_LOGIN_INVALID         = tpl_msg_login_invalid.innerText;
const MSG_LOGIN_ERROR           = tpl_msg_login_error.innerText;
const MSG_UNSUPPORTED_OPERATION = tpl_msg_unsupported_op.innerText;
const MSG_ERROR_GENERIC_403     = tpl_msg_error_generic_403.innerText;
const MSG_ERROR_GENERIC_404     = tpl_msg_error_generic_404.innerText;

const ENTITY_COPY_SUFFIX        = " (Copy)";
const HINT_SELECT_CLASS         = "<select a class>";
const HINT_SELECT_STUDENT       = "<select a student>";
const HINT_SELECT_SUBJECT       = "<select a subject>";
const HINT_SELECT_CATEGORY      = "<select a category>";
const GRADES_NAME               = ["Sehr Gut (1)", "Gut (2)", "Befriedigend (3)", "Genügend (4)", "Nicht Genügend (5)"];
const GRADE_COLORS              = ["#9dfc62", "#cbfc62", "#fce762", "#fcac62", "#fc6262"];
const GRADE_VALUES              = [1, 2, 3, 4, 5];
const GRADE_SYMBOLS             = ["+", "~+", "~", "~-", "-"];
const GRADE_RANGES              = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5];
const GRADE_RANGES_NORM         = [1.000, 0.875, 0.750, 0.625, 0.500, 0.000];
const CATEGORY_INPUT_CHAR       = ["M", "P", "S"];

const CLICK                              = "click";
const ATTR_USER_TYPE                     = "data-user-type";
const CSS_FORM_INPUT                     = "form-input";
const CSS_FORM_INPUT_INVALID             = "form-input-invalid";
const CSS_FORM_SINGLE_SELECT             = "form-single-select";
const CSS_FORM_SINGLE_SELECT_HIDDEN      = "form-single-select-hidden";
const CSS_FORM_SINGLE_SELECT_CONTAINER   = "form-single-select-container";
const CSS_FORM_SINGLE_SELECT_PLACEHOLDER = "form-single-select-placeholder"
const CSS_FORM_MULTI_SELECT              = "form-multi-select";
const CSS_FORM_MULTI_SELECT_SELECTION    = "form-multi-select-selection";
const CSS_FORM_MULTI_SELECT_ADD          = "form-multi-select-add";
const CSS_FORM_MULTI_SELECT_DEL          = "form-multi-select-del";
const CSS_FORM_WRITABLE                  = "form-writable";
const CSS_FORM_TAG                       = "form-tag";
const CSS_LIST_LINE_NUMBER               = "list-line-number";
const CSS_HIDDEN                         = "hidden";
const CSS_ACTIVE                         = "active";
const CSS_CONTENT_HINT                   = "content-hint";
const CSS_DELETE_HOVER                   = "delete-hover";
const CSS_BTN_REMOVE                     = "btn-remove";
const CSS_USER                           = "user";
const CSS_TOAST_MSG                      = "toast-msg";
const CSS_TOAST_MSG_BAR                  = "toast-msg-bar";
const CSS_LOGIN_BACKGROUND               = "login-background";
const CSS_COMPETENCE_LIST_NONE           = "competence-list-status-none";
const CSS_COMPETENCE_LIST_PASSED         = "competence-list-status-passed";
const CSS_COMPETENCE_LIST_FAILED         = "competence-list-status-failed";
const HTML_DEL_SYMBOL                    = "\u274C";

const EMPTY_GROUP = { name: "", owner: null, subject: null, students: [], teachers: [], grades: [] };

const EMPTY_CATEGORY = {
	name: "",
	lbvo: null,
	weight: 0.0,
	weight_time: 0.0,
	weight_max: 0.0,
	must_pass: false,
	subjects: [],
	input: 0,
	output: 0,
	calc: 0,
	owner: null,
	ranges: [0, 0, 0, 0, 0],
	public: false
};

let body = document.getElementsByTagName("body").item(0);
let content = document.getElementById("content");
let data, updates;

async function on_load() {
	// resume session

	let response = await fetch(API_RESUME, { method: "POST" });

	if (response.status === 403) {
		login_init();
		console.debug("session was not resumed: " + await response.text());
		return;
	} else if (response.status !== 200) {
		login_init();
		toast_msg_show("error", "Failed to resume session");
		return;
	}

	// get user data

	data = await response.json();
	EMPTY_GROUP.owner = data.id;
	EMPTY_CATEGORY.owner = data.id;

	if (data.ty === USER_TYPE_STUDENT) {
		data.groups.forEach(v => grade_calc_final(v.grades, data.schug82e));
		data.groups.sort((a, b) => {
			let na = a.subject.toLowerCase();
			let nb = b.subject.toLowerCase();
			return (na < nb) ? -1 : (na > nb) ? 1 : 0;
		});
	} else if (data.ty === USER_TYPE_TEACHER) {
		for (const group of data.groups)
			for (const grades of group.grades)
				grade_calc_final(grades.grades, lookup(data.students, grades.student).schug82e);

		for (const student of data.students)
			student.id_class = lookup(data.classes, student.class).students.indexOf(student.id) + 1;

		data.groups.sort(v => v.name);
		data.categories.sort(v => v.name);
	}

	// misc

	user_img.src = data.image ? data.image : DEFAULT_USER;
	user_name.textContent = data.name;

	if (data.ty === USER_TYPE_ADMIN)
		body.setAttribute(ATTR_USER_TYPE, "admin");
	else if (data.ty === USER_TYPE_TEACHER)
		body.setAttribute(ATTR_USER_TYPE, "teacher");
	else if (data.ty === USER_TYPE_STUDENT)
		body.setAttribute(ATTR_USER_TYPE, "student");

	// hide all dropdowns when the body is clicked
	body.addEventListener(CLICK, e => {
		for (const element of document.getElementsByClassName(CSS_FORM_SINGLE_SELECT))
			if (element !== e.target.parentElement && !element.parentElement.parentElement.classList.contains(CSS_FORM_MULTI_SELECT))
				element.classList.add(CSS_FORM_SINGLE_SELECT_HIDDEN);

		for (const element of document.getElementsByClassName(CSS_FORM_MULTI_SELECT))
			if (element !== e.target.parentElement.parentElement && element.getElementsByClassName(CSS_FORM_MULTI_SELECT_ADD)[0] !== e.target) {
				element.getElementsByClassName(CSS_FORM_SINGLE_SELECT_CONTAINER)[0].classList.add(CSS_HIDDEN);
				element.getElementsByClassName(CSS_FORM_MULTI_SELECT_ADD)[0].classList.remove(CSS_HIDDEN);
			}
	});

	// init header menu items
	document.querySelectorAll("[data-push-url]").forEach(e => e.addEventListener(CLICK, function () {
		window.history.pushState({}, "", this.getAttribute("data-push-url"));
		on_popstate();
	}));

	window.addEventListener("popstate", on_popstate);
	user_logout.addEventListener(CLICK, on_logout);
	await on_popstate();
}

async function on_popstate() {
	try {
		document.querySelectorAll(".header-menu[data-push-url]").forEach(v => v.classList.remove("active"));
		document.querySelectorAll(".header-menu[data-push-url=\"" + window.location.pathname + "\"]").forEach(v => v.classList.add("active"));

		let path = window.location.pathname;
		let query = new URLSearchParams(window.location.search);

		if (data.ty === USER_TYPE_TEACHER)
			switch (path.substring(0, path.indexOf("/", 1) === -1 ? path.length : path.indexOf("/", 1))) {
				case "/":
					await push_state("/group");
					break;
				case "/group":
					if (query.has("s"))
						await group_student_init();
					else if (query.has("d"))
						await group_grades_init();
					else
						await group_init();
					break;
				case "/category":
					await category_init();
					break;
				default:
					await push_state("/");
			}
		else if (data.ty === USER_TYPE_STUDENT)
			switch (path) {
				case "/":
					await student_home_init();
					break;
				case "/group":
					await subject_detail_init();
					break;
				default:
					await push_state("/");
			}
		else if (data.ty === USER_TYPE_ADMIN) {
			header_logo_admin.classList.remove("hidden");

			switch (path.substring(0, path.indexOf("/", 1) === -1 ? path.length : path.indexOf("/", 1))) {
				case "/":
					admin_home_init();
					break;
				case "/feed":
					admin_feed_init()
					break;
				case "/data":
					admin_data_init()
					break;
				case "/metrics":
					admin_metrics_init()
					break;
				case "/db":
					admin_db_init()
					break;
				default:
					await push_state("/");
			}
		} else {
			console.error("login response has invalid user type: " + data.ty);
			toast_msg_show("error", "Invalid user type");
		}
	} catch (e) {
		console.error(e);
		content_msg_show("Failed to display page: " + e);
	}
}

async function on_login() {
	if (login_usr.value.trim().length === 0 || login_pwd.value.trim().length === 0) {
		toast_msg_show("error", MSG_LOGIN_EMPTY)
		return;
	}

	let response = await fetch(API_LOGIN, {
		method: "POST",
		body:   JSON.stringify({ usr: login_usr.value, pwd: login_pwd.value, rem: login_rem.checked })
	});

	if (response.status === 403) {
		console.debug("user was not logged in: invalid username or password");
		toast_msg_show("error", MSG_LOGIN_INVALID);
	} else if (response.status !== 200) {
		let text = await response.text();
		console.debug("user was not logged in: " + text);
		toast_msg_show("error", MSG_LOGIN_ERROR + " (" + text + ")");
	} else {
		console.debug("user logged in as " + login_usr.value.trim() + ", reloading");
		window.location.reload(false);
	}
}

async function on_logout() {
	await fetch(API_LOGOUT, { method: "POST"});
	window.history.replaceState(null, null, "/");
	window.location.reload(false);
}

function login_init() {
	set_template(content, tpl_login);
	content.classList.add(CSS_LOGIN_BACKGROUND);
	login_usr.addEventListener("keypress", e => { if (e.keyCode === 13) on_login(); });
	login_pwd.addEventListener("keypress", e => { if (e.keyCode === 13) on_login(); });
	btn_login.addEventListener(CLICK, on_login);
}

async function student_home_init() {
	set_template(content, tpl_student_home);

	//student_home_select_diagram.classList.add(CSS_ACTIVE);
	student_home_select_diagram.addEventListener(CLICK, () => subjects_chart.scrollIntoView(true));
	student_home_select_subjects.addEventListener(CLICK, () => subject_list.scrollIntoView(true));
	student_home_select_history.addEventListener(CLICK, () => history_list.scrollIntoView(true));

	// init chart

	grades_chart_init(subjects_chart, [...data.groups.map(group => { return {
			label:       lookup(data.subjects, group.subject).short_name + " (" + lookup(data.teachers, group.teacher).name + ")",
			borderColor: chart_color(group.subject, group.teacher),
			fill:        false,
			data:        group.grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.final) }}),
			cubicInterpolationMode: "monotone"
		}; }), ...data.groups.map(group => { return {
			label:       lookup(data.subjects, group.subject).short_name + " (" + lookup(data.teachers, group.teacher).name + ") Scattered",
			type:        "scatter",
			borderColor: chart_color(group.subject, group.teacher),
			fill:        false,
			data:        group.grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.grade) }}),
			cubicInterpolationMode: "monotone"
		};
	})]);

	// init subjects

	for (const group of data.groups)
		form_row_build()
			.append_text(lookup(data.subjects, group.subject).short_name)
			.append_text(lookup(data.teachers, group.teacher).name)
			.append_text("" + group.grades.length)
			.append_text((
				group.final_grade_override ? group.final_grade_override
					: grade_to_number(group.grades[group.grades.length - 1].final)
			).toFixed(1))
			.on_click(() => push_state("/group?s=" + group.subject + "&t=" + group.teacher))
			.finish(subject_list);

	// init history

	let grades = new Map();

	for (const group of data.groups)
		for (const grade of group.grades)
			grades.set(date_form_reql(grade.date), { subject: group.subject, teacher: group.teacher, ...grade });

	for (const grade of Array.from(grades.values()).reverse())
		form_row_build()
			.append_text(date_form_reql(grade.date).toLocaleDateString())
			.append_text(lookup(data.subjects, grade.subject).short_name)
			.append_text(lookup(data.teachers, grade.teacher).name)
			.append_text(lookup(data.lbvo_categories, lookup(data.categories, grade.category).lbvo).short_name)
			.append_text(grade_to_string(grade.grade, lookup(data.categories, grade.category)))
			.append(data.last_login && data.last_login.epoch_time > grade.date.epoch_time
				? form_text_create("")
				: form_text_create("NEW", [CSS_FORM_TAG]))
			.finish(history_list);
}

async function subject_detail_init() {
	set_template(content, tpl_student_subject_detail);
	let query = new URLSearchParams(window.location.search);
	let group = data.groups.find(e => e.subject === query.get("s") && e.teacher === query.get("t"));

	// init menu

	for (const group_ of data.groups) {
		let subject = lookup(data.subjects, group_.subject).short_name;
		let teacher = lookup(data.teachers,  group_.teacher).name;
		let nav     = document.createElement("div");
		nav.textContent = subject + " (" + teacher + ")";
		nav.addEventListener(CLICK, () => push_state("/group?s=" + group_.subject + "&t=" + group_.teacher));

		if (group_ === group)
			nav.classList.add(CSS_ACTIVE);

		subject_select.appendChild(nav);
	}

	// check if group exists

	if (!group) {
		content_msg_show("Subject with teacher not found");
		return;
	}

	let subject = lookup(data.subjects, group.subject);

	// properties

	if (group.grades)
		if (group.final_grade_override)
			final_grade.textContent = group.final_grade_override.toFixed(1) + " ("
				+ grade_to_number(group.grades[group.grades.length - 1].final).toFixed(1) + ")";
		else
			final_grade.textContent = grade_to_number(group.grades[group.grades.length - 1].final).toFixed(1);

	// init actions

	subject_back.addEventListener(CLICK, () => push_state("/"));

	// init competences

	if (!subject.competences || subject.competences.length === 0 || !data.schug82e) {
		competence_list.previousSibling.remove();
		competence_list.previousSibling.remove();
		competence_list.remove();
	} else for (let i = 0; i < subject.competences.length; i++) {
		create_competences_list(i, subject, group.grades[group.grades.length - 1], competence_list);
		subject_grades_list_competences.appendChild(form_text_create(i + 1));
	}

	// init grades

	if (group.grades)
		for (const grade of group.grades) {
			let competences = document.createElement("div");
			competences.classList.add("grade-competence-container")

			for (let i = 0; i < subject.competences.length; i++) {
				let div = document.createElement("div");
				div.classList.add("grade-competence-checkbox", (grade.comp_passed & (1 << i)) !== 0 ? "grade-competence-passed"
					: (grade.comp_failed & (1 << i)) !== 0 ? "grade-competence-failed"
					: "grade-competence-none");
				competences.append(div);
			}

			form_row_build()
				.append_text(date_form_reql(grade.date).toLocaleDateString())
				.append_text(lookup(data.lbvo_categories, lookup(data.categories, grade.category).lbvo).short_name)
				.append_text(grade.comment_pub ? grade.comment_pub : "-")
				.append_text(grade_to_string(grade.grade, lookup(data.categories, grade.category)))
				.append(competences)
				.append_text(grade_to_number(grade.final).toFixed(1))
				.finish(subject_grades_list)
		}

	// init chart

	grades_chart_init(subject_chart, [
		{
			label:       "Final Grade",
			borderColor: chart_color(group.subject, group.teacher),
			fill:        false,
			data:        group.grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.final) }}),
			cubicInterpolationMode: "monotone"
		},
		{
			label:       "Grades Scattered",
			type:        "scatter",
			borderColor: chart_color(group.subject, group.teacher),
			fill:        false,
			data:        group.grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.grade) }}),
			cubicInterpolationMode: "monotone"
		}
	]);
}

async function group_init() {

	// get group

	let query = new URLSearchParams(window.location.search);
	let group = data.groups.find(e => e.id === query.get("g"));
	set_template(content, tpl_teacher_group);

	if (query.get("g") === "null") {
		group = EMPTY_GROUP;
		group.owner = data.id;
		group_new.classList.add(CSS_ACTIVE);
	}

	// init menu

	form_menu_search_init(groups_search, group_select);
	group_new.addEventListener(CLICK, () => push_state("/group?g=null"));

	for (const group_data of data.groups) {
		let nav = document.createElement("div");
		nav.textContent = group_data.name;
		nav.addEventListener(CLICK, () => push_state("/group?g=" + group_data.id));

		if (group && group_data.id === group.id)
			nav.classList.add(CSS_ACTIVE);

		if (group_data.owner === data.id)
			group_select.insertBefore(nav, groups_public);
		else
			group_select.appendChild(nav);
	}

	// check if group selected

	if (!group) {
		form_content_hint_init(group_edit, MSG_SELECT_GROUP);
		return;
	}

	updates = JSON.parse(JSON.stringify(group));

	// init group properties

	let authorized = group.owner === data.id;
	let grades_data = group_get_grades_data(group);
	group_name.textContent = group.name;
	group_subject.classList.add(CSS_FORM_SINGLE_SELECT_HIDDEN);
	form_subpages_init();

	// init subject list

	if (!group.subject)
		group_subject.append(form_single_select_placeholder_create(HINT_SELECT_SUBJECT));

	for (const subject of data.subjects) {
		if (!data.subjects_classes.find(v => v[0] === group.subject))
			continue;

		let row         = document.createElement("div");
		row.textContent = subject.short_name;

		if (authorized) {
			row.addEventListener(CLICK, form_single_select_on_select);
			row.addEventListener(CLICK, () => updates.subject = subject.id);
		}

		if (subject.id === group.subject && group_subject.children.length > 0)
			group_subject.insertBefore(row, group_subject.children[0]);
		else
			group_subject.appendChild(row);
	}

	// init teacher list

	for (const teacher of data.teachers) {
		if (teacher.id === group.owner || !teacher.subjects_classes.find(v => v[0] === group.subject))
			continue;

		let row = document.createElement("div");
		row.textContent = teacher.name;
		row.addEventListener(CLICK, () => group_on_teacher_add.call(row, group, teacher));
		group_teachers_select.appendChild(row);

		if (group.teachers.indexOf(teacher.id) !== -1)
			group_on_teacher_add.call(row, group, teacher, true);
	}

	// init student list

	for (const student_id of group.students) {
		let student = lookup(data.students, student_id);

		if (!student) {
			console.error("Invalid student: " + student_id);
			continue;
		}

		let class_data = lookup(data.classes, student.class);
		let grades = grades_data.by_student.get(student_id);

		form_row_build()
			.append_text(class_data.name)
			.append_text(student.id_class)
			.append_text(student.name)
			.append_text(grades ? grade = grade_to_number(grades[grades.length - 1].final).toFixed(1) : "-")
			.on_click(() => push_state("/group?g=" + group.id + "&s=" + student.id))
			.on_delete(!authorized ? undefined : () => updates.students.remove_eq(student.id))
			.finish(group_students_list, student_add);
	}

	new Chart(group_students_chart.getContext('2d'), {
		type: "bar",
		data: {
			labels: GRADES_NAME,
			datasets: [{ label: "Grades", data:  grades_data.aggregated.counts }]
		},
		options: {
			animation: false,
			responsive: true,
			scales: {
				xAxes: [{ gridLines: { display: false } }],
				yAxes: [{
					ticks: {
						beginAtZero: true,
						max: group.students.length,
						stepSize: 1
					}
				}]
			}
		}
	});

	// init grade list

	for (const grades of grades_data.by_date) {
		let category_data = lookup(data.categories, grades.category);

		if (!category_data) {
			console.error("Invalid category: " + grades.category);
			continue;
		}

		let grades_data = grades;
		form_row_build()
			.append_text(date_form_reql(grades.date).toLocaleDateString())
			.append_text(category_data.name)
			.append_text(grades.count)
			.append_text(grade_to_number(grades.avg).toFixed(1))
			.on_click(() => push_state("/group?g=" + group.id + "&d=" + grades.date.epoch_time + "&c=" + grades.category))
			.on_delete(() => {
				for (const grades of updates.grades)
					grades.grades = grades.grades.filter(v => !grades_data.ids.contains(v.id));
			})
			.finish(group_grades_list, grades_add);
	}

	new Chart(group_grades_chart.getContext('2d'), {
		type: "line",
		data: {
			datasets: [
				{
					label:       "Average",
					type:        "line",
					fill:        false,
					tension:     0,
					borderColor: chart_color(group.subject),
					data:        grades_data.by_date.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.avg) }})
				},
				{
					label:       "Scattered",
					type:        "scatter",
					borderColor: chart_color(group.subject),
					data:        group.grades.flatMap(v => v.grades)
						.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.grade) }})
				},
				{
					label:       "Lower Quartile",
					type:        "line",
					fill:        false,
					tension:     0,
					borderDash:  [10, 10],
					borderColor: "#C0C0C0",
					data:        grades_data.by_date.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.quartiles[0]) }})
				},
				{
					label:       "Middle Quartile",
					type:        "line",
					fill:        false,
					tension:     0,
					borderDash:  [10, 10],
					borderColor: "#C0C0C0",
					data:        grades_data.by_date.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.quartiles[1]) }})
				},
				{
					label:       "Upper Quartile",
					type:        "line",
					fill:        false,
					tension:     0,
					borderDash:  [10, 10],
					borderColor: "#C0C0C0",
					data:        grades_data.by_date.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.quartiles[2]) }})
				}
			]
		},
		options: {
			animation: false,
			responsive: true,
			plugins: {
				title: { display: true, text: "Grades" }
			},
			scales: {
				xAxes: [{ type: "time", time: { unit: "day" } }],
				yAxes: [{ ticks: {
					reverse: true,
					min: 0.5,
					max: 5.5,
					suggestedMin: 0.5,
					suggestedMax: 5.5
				}}]
			}
		}
	});

	new Chart(group_grades_chart_number.getContext('2d'), {
		type: "bar",
		data: {
			datasets: [4, 3, 2, 1, 0].map(i => { return {
				label: GRADES_NAME[i],
				borderColor: GRADE_COLORS[i],
				backgroundColor: GRADE_COLORS[i],
				data:  grades_data.by_date.map(v => { return { x: date_form_reql(v.date), y: v.counts[i] }})
			}; })
		},
		options: {
			animation: false,
			responsive: true,
			plugins: {
				title: { display: true, text: "Number of Grades by Day" }
			},
			scales: {
				xAxes: [{ type: "time", time: { unit: "day" }, stacked: true, gridLines: { display: false } }],
				yAxes: [
					{
						stacked: true,
						gridLines: { display: false },
						ticks: {
							beginAtZero: true,
							stepSize: 1,
							max: group.students.length
						}
					}
				]
			}
		}
	});

	// init group actions

	group_action_copy.addEventListener(CLICK,   () => group_on_copy(group));
	form_field_init(group_name, "name");

	if (authorized) {
		group_actions.style.visibility = "visible";
		group_name.classList.add(CSS_FORM_WRITABLE);
		group_subject.classList.add(CSS_FORM_WRITABLE);
		group_teachers.classList.add(CSS_FORM_WRITABLE);
		student_add.classList.remove(CSS_HIDDEN);
		group_teachers_add.classList.remove(CSS_HIDDEN);
		group_teachers_add.addEventListener(CLICK, () => form_multi_select_on_add(group_teachers_add, group_teachers_select));
		group_action_import.addEventListener(CLICK, () => group_on_import(group));
		group_action_import_template.addEventListener(CLICK, () => group_on_import_template(group));
		group_action_export.addEventListener(CLICK, () => group_on_export(group));
		group_action_reset.addEventListener(CLICK, group_init);
		group_action_save.addEventListener(CLICK, () => group_on_save(group));
		group_action_delete.addEventListener(CLICK, () => group_on_delete(group));
		student_add.addEventListener(CLICK, group_on_student_add);
	}

	if (authorized || group.teachers.indexOf(data.id) !== -1) {
		grades_add.classList.remove(CSS_HIDDEN);
		grades_add.addEventListener(CLICK, () => push_state("/group?g=" + group.id + "&d=null&c=null"));
	}
}

function group_get_grades_data(group) {
	let map = new Map();
	let counts = [0, 0, 0, 0, 0];

	group.grades.forEach(grade => {
		map.set(grade.student, grade.grades);
		counts[Math.round(grade_to_number(grade.grades[grade.grades.length - 1].final)) - 1]++;
	});

	return {
		aggregated: {
			counts: counts
		},
		by_student: map,
		by_date: group.grades.flatMap(v => v.grades)
			.groupBy(
				v => v.date.epoch_time + v.category,
				(e, grade) => {
					e.ids.push(grade.id);
					e.date = grade.date;
					e.category = grade.category;
					e.count += 1;
					e.avg += grade.grade;
					e.quartiles.push(grade.grade);
					e.counts[Math.min(Math.round(grade_to_number(grade.grade)) - 1, 4)]++;
				},
				() => { return {
					ids:       [],
					date:      null,
					category:  null,
					count:     0,
					avg:       0,
					counts:    [0, 0, 0, 0, 0],
					quartiles: []
				}; }
			)
			.map(e => {
				e.avg /= e.count;
				e.quartiles.sort();
				e.quartiles = [0.25, 0.50, 0.75].map(p => {
					let r = p * (e.quartiles.length - 1);
					let i = Math.floor(r);
					return e.quartiles[i] + (r - i) * (e.quartiles[i + 1] - e.quartiles[i]);
				});
				return e;
			})
			.sort((a, b) => (a.date < b.date) ? -1 : (a.date > b.date) ? 1 : 0)
	};
}

function group_on_import_template(group) {
	let file = "Date\nCategory\nStudents";

	for (const student of group.students)
		file += "\n" + lookup(data.students, student).name;

	download_file("grades.csv", "data:text/csv;charset=utf-8," + encodeURIComponent(file));
}

function group_on_import(group) {
	upload_file(async () => {
		if (input.files.length < 1) {
			toast_msg_show("error", "No file selected");
			return;
		}

		let file = input.files[0];

		if (file.type !== "text/csv") {
			toast_msg_show("error", "Invalid file type");
			return;
		} else if (file.size === 0) {
			toast_msg_show("error", "File is empty");
			return;
		}

		file = await file.text();
		let lines = file.split("\n");

		let dates = lines[0].split(/[,;]/).slice(1).map(v => Date.parse(v));
		let categories = lines[1].split(/[,;]/).slice(1).map(name => data.categories.find(v => v.name === name));

		for (const line of lines.slice(3)) {
			let grades = line.split(/[,;]/);
			let student = data.students.find(v => v.name === grades[0]);
			let grades_data = updates.grades.find(v => v.student === student.id);

			for (let i = 1; i < grades.length; i++)
				grades_data.grades.push({
					date:     dates[i - 1],
					category: categories[i - 1],
					value:    grades[i]
				});
		}

		await group_on_save(group);
	});
}

function group_on_export(group) {
	let file = "";
	let students = new Map();
	let dates = new Map();

	for (const student_id of group.students)
		students.set(lookup(data.students, student_id).name, student_id);

	for (const grades of group.grades)
		for (const grade of grades.grades) {
			let key = String(grade.date.epoch_time).padStart(16, '0') + grade.category;

			if (!dates.has(key))
				dates.set(key, { date: grade.date.epoch_time, category: grade.category, students: new Map() });

			dates.get(key).students.set(grades.student, grade.value);
		}

	file += "Date";
	for (const grades of dates.values())
		file += ";" + date_form_reql({ epoch_time: grades.date }).toLocaleDateString();

	file += "\nCategory";

	for (const grades of dates.values())
		file += ";" + lookup(data.categories, grades.category).name;

	file += "\nStudents";

	for (const student of students) {
		file += "\n" + student[0];

		for (const grades of dates.values()) {
			let value = grades.students.get(student[1]);
			file += ";" + (value ? value : "-");
		}
	}

	download_file("grades.csv", "data:text/csv;charset=utf-8," + encodeURIComponent(file));
}

async function group_on_save(group) {
	if (updates.name.trim().length === 0) {
		group_name.classList.add(CSS_FORM_INPUT_INVALID);
		return;
	}

	let body = {};

	if (group.name !== updates.name) body.name = updates.name;
	if (group.subject !== updates.subject) body.subject = updates.subject;
	let teachers = seq_diff(group.teachers, updates.teachers);
	if (teachers) body.teachers = teachers;
	let students = seq_diff(group.students, updates.students);
	if (students) body.students = students;

	for (let i = 0; i < updates.grades.length; i++) {
		let old = group.grades[i];
		let upd = updates.grades[i];

		for (const grade of upd.grades)
			if (!grade.id) {
				if (!body.grades)
					body.grades = {};
				if (!body.grades.add)
					body.grades.add = [];
				body.grades.add.push(grade);
			}

		for (const grade of old.grades) {
			let grade_upd = upd.grades.find(v => v.id === grade.id);

			if (!grade_upd) {
				if (!body.grades)
					body.grades = {};
				if (!body.grades.del)
					body.grades.del = [];
				body.grades.del.push(grade.id);
			} else if (JSON.stringify(grade) !== JSON.stringify(grade_upd)) {
				if (!body.grades)
					body.grades = {};
				if (!body.grades.chg)
					body.grades.chg = [];

				let chg = { id: grade.id };

				if (grade.value !== grade_upd.value) chg.value = grade_upd.value;
				if (grade.date !== grade_upd.date) chg.date = grade_upd.date;
				if (grade.category !== grade_upd.category) chg.category = grade_upd.category;
				if (grade.comment_pub !== grade_upd.comment_pub) chg.comment_pub = grade_upd.comment_pub;
				if (grade.comment_pri !== grade_upd.comment_pri) chg.comment_pri = grade_upd.comment_pri;
				if (grade.comp_passed !== grade_upd.comp_passed) chg.comp_passed = grade_upd.comp_passed;
				if (grade.comp_failed !== grade_upd.comp_failed) chg.comp_failed = grade_upd.comp_failed;

				body.grades.chg.push(chg);
			}
		}
	}

	// TODO update grades edited and new grades id

	console.debug(body);

	if (updates.id) {
		await update_push("PATCH", API_UPDATE_GROUP + "?id=" + updates.id, MSG_UPDATE_GROUP, MSG_UPDATE_GROUP_ERROR, body);
		data.groups[data.groups.indexOf(group)] = updates;
		await on_popstate();
	} else {
		let response = await update_push("POST", API_UPDATE_GROUP, undefined, MSG_UPDATE_GROUP_ERROR);
		await update_push("PATCH", API_UPDATE_GROUP + "?id=" + response.id, MSG_UPDATE_GROUP, MSG_UPDATE_GROUP_ERROR, body);
		updates.id = response.id;
		data.groups.push(updates);
		await push_state("/group?g=" + response.id);
	}

	updates = JSON.parse(JSON.stringify(updates));
}

async function group_on_copy(group) {
	delete updates.id;
	updates.owner = data.id;
	updates.name += ENTITY_COPY_SUFFIX;
	await group_on_save(EMPTY_GROUP);
}

async function group_on_delete(group) {
	await update_push("DELETE", API_UPDATE_GROUP + "?id=" + group.id, MSG_UPDATE_GROUP, MSG_UPDATE_GROUP_ERROR);
	data.groups.splice(data.groups.indexOf(group), 1);
	await push_state("/group");
}

function group_on_teacher_add(group, teacher, shadow) {
	if (!shadow)
		updates.teachers.push(teacher.id);

	let div = document.createElement("div");
	let img = document.createElement("img");
	let usr = document.createElement("div");
	let del = document.createElement("div");

	div.classList.add(CSS_FORM_MULTI_SELECT_SELECTION, CSS_USER);
	img.src = teacher.image ? teacher.image : DEFAULT_USER;
	usr.textContent = teacher.name;
	del.textContent = "\u274C";
	del.classList.add(CSS_FORM_MULTI_SELECT_DEL);
	del.addEventListener(CLICK, () => {
		div.remove();
		this.classList.remove(CSS_HIDDEN);
		updates.teachers.remove_eq(teacher.id);

		if (document.querySelectorAll(".form-multi-select-selection").length === 0)
			group_teachers_none.classList.remove(CSS_HIDDEN);
	});

	div.appendChild(img);
	div.appendChild(usr);
	if (group.owner === data.id)
		div.appendChild(del);
	form_multi_select_on_select.call(this, group_teachers, group_teachers_add, group_teachers_select, div);
}

function group_on_student_add() {
	let row          = document.createElement("div");
	let class_       = document.createElement("div");
	let class_select = document.createElement("div");
	let id           = document.createElement("div");
	let name         = document.createElement("div");
	let grade        = document.createElement("div");
	let btn_remove   = document.createElement("div");

	id.textContent        = "-";
	name.textContent      = "-";
	grade.textContent     = "-";
	name.classList.add(CSS_FORM_SINGLE_SELECT_CONTAINER);
	class_.classList.add(CSS_FORM_SINGLE_SELECT_CONTAINER);
	class_.appendChild(class_select);

	let class_select_none = document.createElement("div");
	class_select_none.textContent = HINT_SELECT_CLASS;
	class_select_none.classList.add(CSS_FORM_SINGLE_SELECT_PLACEHOLDER);
	class_select_none.addEventListener(CLICK, form_single_select_on_select);
	class_select.appendChild(class_select_none);
	class_select.classList.add(CSS_FORM_SINGLE_SELECT)

	for (const class_ of data.classes) {
		// skip classes where all students are already in the group
		if (!class_.students.find(v => updates.students.indexOf(v) === -1))
			continue;

		let div = document.createElement("div");
		div.textContent = class_.name;
		div.addEventListener(CLICK, form_single_select_on_select);
		div.addEventListener(CLICK, () => {
			if (class_select_none.parentElement)
				class_select_none.remove();

			name.innerHTML = "";
			let name_select = document.createElement("div");
			let name_select_none = document.createElement("div");
			name_select_none.textContent = HINT_SELECT_STUDENT;
			name_select_none.classList.add(CSS_FORM_SINGLE_SELECT_PLACEHOLDER);
			name_select_none.addEventListener(CLICK, form_single_select_on_select);
			name_select.appendChild(name_select_none);
			name_select.classList.add(CSS_FORM_SINGLE_SELECT);

			for (const student of class_.students) {
				if (updates.students.indexOf(student) !== -1)
					continue;

				let student_data = lookup(data.students, student);
				let div = document.createElement("div");
				div.textContent = student_data.name;
				div.addEventListener(CLICK, form_single_select_on_select);
				div.addEventListener(CLICK, () => {
					if (name_select_none.parentElement)
						name_select_none.remove();

					updates.students.push(student);

					class_.innerHTML   = "";
					id.innerHTML       = "";
					name.innerHTML     = "";
					class_.textContent = class_.name;
					id.textContent     = student_data.id_class;
					name.textContent   = student_data.name;
					btn_remove.replaceWith(form_row_remove_create(row, () => updates.students = updates.students.filter(v => v !== student)));
				});

				name_select.appendChild(div)
			}

			name.appendChild(name_select);
		});
		class_select.appendChild(div);
	}

	form_row_remove_init(btn_remove, row);
	row.appendChild(class_);
	row.appendChild(id);
	row.appendChild(name);
	row.appendChild(grade);
	row.appendChild(btn_remove);
	group_students_list.insertBefore(row, student_add);
}

async function group_student_init() {

	// get student

	let query      = new URLSearchParams(window.location.search);
	let group      = data.groups.find(e => e.id === query.get("g"));
	let student_id = query.get("s");
	set_template(content, tpl_teacher_student);

	// check if student valid

	if (!group || !student_id) {
		await push_state("/group");
		return;
	}

	updates = JSON.parse(JSON.stringify(group));

	// init menu

	student_back.addEventListener(CLICK, () => push_state("/group?g=" + group.id));
	form_menu_search_init(students_search, student_select);

	for (const student_id_ of group.students) {
		let student_data = lookup(data.students, student_id_);
		let nav          = document.createElement("div");
		nav.textContent  = student_data.name;
		nav.addEventListener(CLICK, () => push_state("/group?g=" + group.id + "&s=" + student_data.id));

		if (student_id_ === student_id)
			nav.classList.add(CSS_ACTIVE);

		student_select.appendChild(nav);
	}

	// init student properties

	let student        = lookup(data.students, student_id);
	let subject        = lookup(data.subjects, group.subject);
	let class_         = lookup(data.classes, student.class);
	let grades_student = group.grades ? updates.grades.find(v => v.student === student.id) : undefined;
	let grades         = grades_student ? grades_student.grades : undefined;

	student_name.textContent     = class_.name + " " + student.id_class + " " + student.name;
	student_subject.textContent  = subject.name;
	form_input_text_init(student_comment, grades_student.comment_pri, e => grades_student.comment_pri = e.target.textContent.trim());

	// init student actions

	student_action_reset.addEventListener(CLICK, group_student_init);
	student_action_save.addEventListener(CLICK, () => group_on_save(group));
	mark_add.parentElement.addEventListener(CLICK, () => group_student_on_grade_add(student_id, subject));

	if (!grades) return;

	// init student properties

	student_final_grade.textContent = grade_to_number(grades[grades.length - 1].final).toFixed(1);
	student_final_grade_override.textContent = grades_student.final_grade ? grades_student.final_grade : "";
	form_input_text_init(student_final_grade_override, grades_student.final_grade_override, e => grades_student.final_grade_override = e.target.textContent.trim());

	if (grades[grades.length - 1].final <= 0.5)
		hint_notice.classList.remove(CSS_HIDDEN);

	// init competence list

	if (!subject.competences || subject.competences.length === 0 || !student.schug82e) {
		competence_list.previousSibling.remove();
		competence_list.previousSibling.remove();
		competence_list.remove();
	} else for (let i = 0; i < subject.competences.length; i++) {
		create_competences_list(i, subject, grades[grades.length - 1], competence_list);
		student_grades_list_competences.appendChild(form_text_create(i + 1));
	}

	// init grade list

	for (const grade of grades) {
		let category_data      = lookup(data.categories, grade.category)
		let category_container = document.createElement("div");
		let category           = document.createElement("div");
		category_container.classList.add(CSS_FORM_SINGLE_SELECT_CONTAINER)
		category.classList.add(CSS_FORM_SINGLE_SELECT, CSS_FORM_SINGLE_SELECT_HIDDEN, CSS_FORM_WRITABLE);

		for (const category_data of data.categories) {
			let row         = document.createElement("div");
			row.textContent = category_data.name;
			row.addEventListener(CLICK, form_single_select_on_select);
			row.addEventListener(CLICK, () => grade.category = category_data.id);

			if (category_data.id === grade.category && category.children.length > 0)
				category.insertBefore(row, category.children[0]);
			else
				category.appendChild(row);
		}

		category_container.appendChild(category);

		form_row_build()
			.append(form_input_date_create(date_form_reql(grade.date), e => grade.date = e.target.textContent.trim()))
			.append(category_container)
			.append(create_grade_input(grade.value, category_data, e => grade.value = e.target.textContent.trim()))
			.append(student.schug82e ? create_competences_input(grade, subject) : form_text_create("-"))
			.append(form_input_text_create(grade.comment_pub, e => grade.comment_pub = e.target.textContent.trim()))
			.append(form_input_text_create(grade.comment_pri, e => grade.comment_pri = e.target.textContent.trim()))
			.append(form_text_create(grade_to_number(grade.grade).toFixed(1)))
			.append(form_text_create(grade_to_number(grade.final).toFixed(1)))
			.append(form_text_create(student.last_login && grade.edited.epoch_time < student.last_login.epoch_time ? "\u2705" : "\u274C"))
			.on_delete(() => grades.remove_eq(grade))
			.finish(group_student_grades_list, mark_add.parentElement);
	}

	// init chart

	grades_chart_init(student_chart, [{
		label:       "Final Grade",
		fill:        false,
		borderColor: chart_color(group.subject, student_id),
		cubicInterpolationMode: "monotone",
		data:        grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.final) }})
	}, {
		label:       "Grades Scattered",
		borderColor: chart_color(group.subject, student_id),
		type:        "scatter",
		data:        grades.map(v => { return { x: date_form_reql(v.date), y: grade_to_number(v.grade) }})
	}]);
}

function group_student_on_grade_add(student, subject) {
	let grade = { id: null };
	let grades = updates.grades.find(v => v.student === student);
	grades.grades.push(grade);

	let div = document.createElement("div");
	div.textContent = HINT_SELECT_CATEGORY;
	let category_select = document.createElement("div");
	category_select.appendChild(div);
	category_select.classList.add(CSS_FORM_SINGLE_SELECT);

	for (const category of data.categories.filter(v => v.subjects.indexOf(updates.subject) !== -1)) {
		let div = document.createElement("div");
		div.textContent = category.name;
		div.addEventListener(CLICK, form_single_select_on_select);
		div.addEventListener(CLICK, () => {
			if (category_select_none.parentElement)
				category_select_none.remove();
			grade.category = category.id;
		});
		category_select.appendChild(div);
	}

	form_row_build()
		.append(form_input_date_create(new Date(), e => grade.date = e.target.textContent.trim()))
		.append(category_select)
		.append(form_input_text_create("", e => grade.value = e.target.textContent.trim()))
		.append(create_competences_input({ comp_passed: 0, comp_failed: 0 }, subject))
		.append(form_input_text_create("", e => grade.comment_pub = e.target.textContent.trim()))
		.append(form_input_text_create("", e => grade.comment_pri = e.target.textContent.trim()))
		.append_text("-")
		.on_delete(() => grades.grades.remove_eq(grade))
		.finish(group_student_grades_list, mark_add.parentElement);
}

async function group_grades_init() {
	// get group

	let query    = new URLSearchParams(window.location.search);
	let group    = lookup(data.groups, query.get("g"));
	let date     = query.get("d");
	let category = query.get("c");
	set_template(content, tpl_teacher_grades);

	if (!group || !date || !category) {
		await push_state("/group");
		return;
	} else if (date === "null" || category === "null")
		grades_new.classList.add(CSS_ACTIVE);

	date = Number(date);
	updates = JSON.parse(JSON.stringify(group));

	let students_map = new Map();
	let grades_by_value = [0, 0, 0, 0, 0];

	for (const grades of group.grades)
		for (const grade of grades.grades)
			if (grade.date.epoch_time === date && grade.category === category) {
				students_map.set(grades.student, grade);
				grades_by_value[Math.min(Math.round(grade_to_number(grade.grade)) - 1, 4)]++;
				break;
			}

	// init menu

	form_menu_search_init(grades_search, grades_select);
	grades_back.addEventListener(CLICK, () => push_state("/group?g=" + group.id));
	grades_new.addEventListener(CLICK, () => push_state("/group?g=" + group.id + "&d=null&c=null"));

	let grades = group_get_grades_data(group).by_date;

	for (const entry of grades) {
		let nav = document.createElement("div");
		nav.textContent = date_form_reql(entry.date).toLocaleDateString() + " " + lookup(data.categories, entry.category).name;
		nav.addEventListener(CLICK, () => push_state("/group?g=" + group.id + "&d=" + entry.date.epoch_time + "&c=" + entry.category));

		if (entry.date.epoch_time === date && entry.category === category)
			nav.classList.add(CSS_ACTIVE);
		grades_select.appendChild(nav);
	}

	// init properties

	for (const category of data.categories) {
		let row         = document.createElement("div");
		row.textContent = category.name;
		row.addEventListener(CLICK, form_single_select_on_select);
		row.addEventListener(CLICK, () => {
			for (const grade of students_map.values())
				grade.category = category.id;
		});

		if (category.id === category && grades_category.children.length > 0)
			grades_category.insertBefore(row, grades_category.children[0]);
		else
			grades_category.appendChild(row);
	}

	form_input_date_init(grades_date, new Date(date * 1000), () => {
		for (const grade of students_map.values())
			grade.date = grades_date.value;
	});

	// init actions

	grades_action_reset.addEventListener(CLICK,  group_grades_init);
	grades_action_save.addEventListener(CLICK,   () => group_on_save(group));
	grades_action_delete.addEventListener(CLICK, () => {
		for (const grades of updates.grades) {
			let grade = students_map.get(grades.student);
			if (grade)
				grades.grades.remove_eq(grade)
		}
		push_state("/group");
	});

	// grades list

	let subject = lookup(data.subjects, group.subject);

	if (subject.competences && subject.competences.length > 0 && group.students.find(v => lookup(data.students, v).schug82e))
		for (let i = 0; i < subject.competences.length; i++)
			student_grades_list_competences.appendChild(form_text_create(i + 1));

	for (const student_id of group.students) {
		let grade  = students_map.get(student_id);
		let student = lookup(data.students, student_id);
		form_row_build()
			.append_text(student.name)
			.append(create_grade_input(grade ? grade.value : null, category ? lookup(data.categories, category): null, e => grade.value = e.target.textContent.trim()))
			.append(student.schug82e ? create_competences_input(grade, subject) : form_text_create(""))
			.append(form_input_text_create(grade ? grade.comment_pub : null, e => grade.comment_pub = e.target.textContent.trim()))
			.append(form_input_text_create(grade ? grade.comment_pri : null, e => grade.comment_pri = e.target.textContent.trim()),)
			.append_text(grade ? grade_to_number(grade.grade).toFixed(1) : "-")
			.append_text(grade ? (student.last_login && grade.edited.epoch_time < student.last_login.epoch_time ? "\u2705" : "\u274C") : "-")
			.on_delete(() => {
				if (grade)
					for (const grades of updates.grades)
						if (grades.student === student_id)
							grades.grades.remove_eq(grade);
				return false;
			})
			.finish(group_grades_grades_list);
	}

	if (grades_by_value[4] >= group.students.length / 2)
		hint_redo_exam.classList.remove(CSS_HIDDEN);

	// chart

	new Chart(grades_chart.getContext('2d'), {
		type: "doughnut",
		data: {
			labels:   GRADES_NAME,
			datasets: [{
				label: "Grades",
				borderColor: "#00000000",
				backgroundColor: GRADE_COLORS,
				data: grades_by_value
			}]
		},
		options: {
			animation: false,
			responsive: true,
			plugins: {
				legend: { position: 'top' },
				title: { display: true, text: "Grades by Value" }
			}
		}
	});
}

async function category_init() {

	// get category

	let query    = new URLSearchParams(window.location.search);
	let category = data.categories.find(e => e.id === query.get("c"));
	set_template(content, tpl_teacher_categories);

	if (query.get("c") === "null") {
		category = EMPTY_CATEGORY;
		category.owner = data.id;
		category_new.classList.add(CSS_ACTIVE);
	}

	// init menu

	form_menu_search_init(categories_search, category_select);
	category_new.addEventListener(CLICK, () => push_state("/category?c=null"));

	for (const category_data of data.categories) {
		let nav = document.createElement("div");
		nav.textContent = category_data.name;
		nav.addEventListener(CLICK, () =>  push_state("/category?c=" + category_data.id));

		if (category && category_data.id === category.id)
			nav.classList.add(CSS_ACTIVE);

		if (category_data.owner === data.id)
			category_select.insertBefore(nav, categories_public);
		else
			category_select.appendChild(nav);
	}

	// check if category exists

	if (!category) {
		form_content_hint_init(category_edit, MSG_SELECT_CATEGORY);
		return;
	}

	updates = JSON.parse(JSON.stringify(category));

	// init category actions

	let authorized = category.owner === data.id;

	category_action_copy.addEventListener(CLICK, () => category_on_copy(category));

	if (authorized) {
		category_actions.style.visibility = "visible";
		category_action_reset.addEventListener(CLICK, category_init);
		category_action_save.addEventListener(CLICK, () => category_on_save(category));
		category_action_delete.addEventListener(CLICK, () => category_on_delete(category));
	}

	// init category properties

	form_input_text_init(category_name, category.name);
	form_input_text_init(category_weight, Math.round(category.weight * 100) + "%");
	form_input_text_init(category_weight_time, Math.round(category.weight_time * 100) + "%");
	form_input_text_init(category_weight_max, Math.round(category.weight_max * 100) + "%");
	form_input_text_init(category_range1, category.ranges[0]);
	form_input_text_init(category_range2, category.ranges[1]);
	form_input_text_init(category_range3, category.ranges[2]);
	form_input_text_init(category_range4, category.ranges[3]);
	form_input_text_init(category_range5, category.ranges[4]);
	form_input_checkbox_init(category_must_pass, category.must_pass, authorized
		? () => updates.must_pass = category_must_pass.checked : undefined);
	form_input_checkbox_init(category_public, category.public, authorized
		? () => updates.public = category_public.checked : undefined);
	category_input_type.insertBefore(category_input_type.children[category.input], category_input_type.children[0]);
	category_output_type.insertBefore(category_output_type.children[category.output], category_output_type.children[0]);
	category_calculation.insertBefore(category_calculation.children[category.calc], category_calculation.children[0]);

	if (authorized) {
		form_input_text_make_writable(category_name, () => updates.name = category_name.textContent.trim());
		form_input_text_make_writable(category_weight, () => updates.weight = form_field_convert_percent(category_weight));
		form_input_text_make_writable(category_weight_time, () => updates.weight_time = form_field_convert_percent(category_weight_time));
		form_input_text_make_writable(category_weight_max, () => updates.weight_max = form_field_convert_percent(category_weight_max));
		form_input_text_make_writable(category_range1, () => updates.ranges[0] = Number(category_range1.textContent.trim()));
		form_input_text_make_writable(category_range2, () => updates.ranges[1] = Number(category_range2.textContent.trim()));
		form_input_text_make_writable(category_range3, () => updates.ranges[2] = Number(category_range3.textContent.trim()));
		form_input_text_make_writable(category_range4, () => updates.ranges[3] = Number(category_range4.textContent.trim()));
		form_input_text_make_writable(category_range5, () => updates.ranges[4] = Number(category_range5.textContent.trim()));
		category_input_type.classList.add(CSS_FORM_WRITABLE);
		category_output_type.classList.add(CSS_FORM_WRITABLE);
		category_calculation.classList.add(CSS_FORM_WRITABLE);

		form_single_select_init(category_input_type, category.input,   e => updates.input  = Number(e.target.getAttribute("data-id")));
		form_single_select_init(category_output_type, category.output, e => updates.output = Number(e.target.getAttribute("data-id")));
		form_single_select_init(category_calculation, category.calc,   e => updates.calc   = Number(e.target.getAttribute("data-id")));
	}

	// lbvo categories

	for (const category_data of data.lbvo_categories) {
		let row = document.createElement("div");
		row.textContent = category_data.short_name;

		if (authorized) {
			row.addEventListener(CLICK, form_single_select_on_select);
			row.addEventListener(CLICK, () => updates.lbvo = category_data.id);
		}

		if (category_data.id === category.lbvo && category_lbvo.children[0])
			category_lbvo.insertBefore(row, category_lbvo.children[0]);
		else
			category_lbvo.appendChild(row);
	}

	if (authorized)
		category_lbvo.classList.add(CSS_FORM_WRITABLE);

	// subjects

	for (const subject of data.subjects) {
		let row = document.createElement("div");
		row.textContent = subject.short_name;
		row.addEventListener(CLICK, () => category_on_subject_add.call(row, subject));
		category_subjects_select.appendChild(row);

		if (category.subjects && category.subjects.indexOf(subject.id) !== -1) {
			category_on_subject_add.call(row, subject, true);
		}
	}

	if (!authorized) {
		document.querySelectorAll("#category_subjects .form-multi-select-del").forEach(e => e.remove());
		category_subjects_add.classList.add(CSS_HIDDEN);
	} else {
		category_subjects.classList.add(CSS_FORM_WRITABLE);
		category_subjects_add.addEventListener(CLICK, () => form_multi_select_on_add(category_subjects_add, category_subjects_select));
	}
}

async function category_on_save(category) {
	let body = {};

	if (category.name !== updates.name) body.name = updates.name;
	if (category.lbvo !== updates.lbvo) body.lbvo = updates.lbvo;
	if (category.weight !== updates.weight) body.weight = updates.weight;
	if (category.weight_time !== updates.weight_time) body.weight_time = updates.weight_time;
	if (category.weight_max !== updates.weight_max) body.weight_max = updates.weight_max;
	if (category.must_pass !== updates.must_pass) body.must_pass = updates.must_pass;
	if (category.public !== updates.public) body.public = updates.public;
	if (category.input !== updates.input) body.input = updates.input;
	if (category.output !== updates.output) body.output = updates.output;
	if (category.calc !== updates.calc) body.calc = updates.calc;
	if (JSON.stringify(category.ranges) !== JSON.stringify(updates.ranges)) body.ranges = updates.ranges;
	let subjects = seq_diff(category.subjects, updates.subjects);
	if (subjects) body.subjects = subjects;

	console.debug(body);

	if (updates.id) {
		await update_push("PATCH", API_UPDATE_CATEGORY + "?id=" + updates.id, MSG_UPDATE_CATEGORY, MSG_UPDATE_CATEGORY_ERROR, body);

		data.categories[data.categories.indexOf(category)] = updates;
		updates = JSON.parse(JSON.stringify(updates));
		await on_popstate();
	} else {
		let response = await update_push("POST", API_UPDATE_CATEGORY, undefined, MSG_UPDATE_CATEGORY_ERROR);
		await update_push("PATCH", API_UPDATE_CATEGORY + "?id=" + response.id, MSG_UPDATE_CATEGORY, MSG_UPDATE_CATEGORY_ERROR, body);
		updates.id = response.id;
		data.categories.push(updates);
		updates = JSON.parse(JSON.stringify(updates));
		await push_state("/category?c=" + response.id);
	}
}

async function category_on_copy(category) {
	delete updates.id;
	updates.owner = data.id;
	updates.name += ENTITY_COPY_SUFFIX;
	await group_on_save(EMPTY_CATEGORY);
}

async function category_on_delete(category) {
	await update_push("DELETE", API_UPDATE_CATEGORY + "?id=" + category.id, MSG_UPDATE_CATEGORY, MSG_UPDATE_CATEGORY_ERROR);
	data.categories.splice(data.categories.indexOf(category), 1);
	await push_state("/category");
}

function category_on_subject_add(subject, shadow) {
	if (!shadow)
		updates.subjects.push(subject.id);

	let div = document.createElement("div");
	let del = document.createElement("div");

	del.textContent = HTML_DEL_SYMBOL;
	del.classList.add(CSS_FORM_MULTI_SELECT_DEL);
	del.addEventListener(CLICK, () => {
		div.remove();
		this.classList.remove(CSS_HIDDEN);
		updates.subjects.remove_eq(subject.id);

		if (document.querySelectorAll(".form-multi-select-selection").length === 0) {
			category_subjects_all.classList.remove(CSS_HIDDEN);
		}
	});

	div.textContent = subject.short_name;
	div.classList.add(CSS_FORM_MULTI_SELECT_SELECTION);
	div.appendChild(del);
	form_multi_select_on_select.call(this, category_subjects, category_subjects_add, category_subjects_select, div);
}

function admin_home_init() {
	set_template(content, tpl_admin_home);

	btn_feed.addEventListener("click", () => push_state("/feed"));
	btn_manage_data.addEventListener("click", () => push_state("/data"));
	btn_metrics.addEventListener("click", () => push_state("/metrics"));
	btn_db_admin.addEventListener("click", () => push_state("/db"));
}

function admin_feed_init() {
	set_template(content, tpl_admin_feed);
}

function admin_data_init() {
	set_template(content, tpl_admin_data);

}

function admin_metrics_init() {
	set_template(content, tpl_admin_frame);
	frame.src = "/proxy/metrics";
}

function admin_db_init() {
	set_template(content, tpl_admin_frame);
	frame.src = "/proxy/db";
}

function form_menu_search_init(element, parent) {
	element.addEventListener("input", () => parent
		.querySelectorAll(":not([id])")
		.forEach(e => {
			if (e.textContent.toLowerCase().indexOf(element.value.trim().toLowerCase()) === -1)
				e.classList.add(CSS_HIDDEN);
			else
				e.classList.remove(CSS_HIDDEN);
		}));
}

function form_single_select_init(container, selected, on_select) {
	container.classList.add(CSS_FORM_SINGLE_SELECT_HIDDEN);
	for (let i = 0; i < container.children.length; i++) {
		container.children[i].addEventListener(CLICK, form_single_select_on_select);
		if (on_select)
			container.children[i].addEventListener(CLICK, on_select);
	}
	container.insertBefore(container.children[selected], container.children[0]);
}

function form_single_select_on_select() {
	if (this.parentElement.classList.contains(CSS_FORM_SINGLE_SELECT_HIDDEN)) {
		this.parentElement.classList.remove(CSS_FORM_SINGLE_SELECT_HIDDEN);
	} else {
		this.parentElement.classList.add(CSS_FORM_SINGLE_SELECT_HIDDEN);
		this.parentElement.insertBefore(this, this.parentElement.children[0]);
	}
}

function form_single_select_placeholder_create(text) {
	let div = document.createElement("div");
	div.textContent = text;
	div.classList.add(CSS_FORM_SINGLE_SELECT_PLACEHOLDER);
	div.addEventListener(CLICK, form_single_select_on_select);
	return div;
}

function form_field_init(element, name, convert) {
	element.classList.add(CSS_FORM_INPUT);
	element.contentEditable = "true";
	element.addEventListener("blur", e => updates[name] = convert ? convert(e.target.textContent) : e.target.textContent);
}

function form_input_text_create(init, listener) {
	let div = document.createElement("div");
	form_input_text_init(div, init, listener);
	return div;
}

function form_input_text_init(element, init, listener) {
	element.classList.add(CSS_FORM_INPUT);

	if (init)
		element.textContent = init;

	if (listener)
		form_input_text_make_writable(element, listener)
}

function form_input_text_make_writable(element, listener) {
	element.classList.add(CSS_FORM_WRITABLE);
	element.contentEditable = "true";
	element.addEventListener("blur", listener);
}

function form_input_date_create(init, listener) {
	let div = document.createElement("input");
	div.type = "date";
	form_input_date_init(div, init, listener);
	return div;
}

function form_input_date_init(element, init, listener) {
	element.classList.add(CSS_FORM_INPUT);
	element.value = date_to_input_value(init);

	if (listener) {
		element.classList.add(CSS_FORM_WRITABLE);
		element.addEventListener("change", listener);
	} else {
		element.readOnly = true;
	}
}

function form_input_checkbox_init(element, init, listener) {
	element.checked = init;

	if (listener)
		element.addEventListener(CLICK, listener);
	else
		element.onclick = () => false;
}

function form_multi_select_on_add(add, select) {
	add.classList.add(CSS_HIDDEN);
	select.parentElement.classList.remove(CSS_HIDDEN);
	select.classList.remove(CSS_FORM_SINGLE_SELECT_HIDDEN);
}

function form_multi_select_on_select(container, add, select, element) {
	container.insertBefore(element, add);
	select.parentElement.classList.add(CSS_HIDDEN);
	add.classList.remove(CSS_HIDDEN);
	this.classList.add(CSS_HIDDEN);

	let all = document.querySelector(".form-multi-select-all")
	if (all) all.classList.add(CSS_HIDDEN);
}

function form_text_create(text, classes) {
	let div = document.createElement("div");
	div.textContent = text;

	if (classes)
		div.classList.add(classes);

	return div;
}

function form_content_hint_init(parent, msg) {
	let hint = document.createElement("div");
	hint.textContent = msg;
	hint.classList.add(CSS_CONTENT_HINT);
	parent.innerHTML = "";
	parent.appendChild(hint);
}

function form_subpages_init() {
	for (const e of document.querySelectorAll(".form-subpage-select > *"))
		e.addEventListener(CLICK, function () {
			this.classList.add(CSS_ACTIVE);
			let subpage = this.getAttribute("data-subpage");

			for (const e of document.querySelectorAll("*:not(.form-subpage-select) > [data-subpage]"))
				if (e.getAttribute("data-subpage") === subpage)
					e.classList.remove(CSS_HIDDEN);
				else
					e.classList.add(CSS_HIDDEN);

			for (const e of document.querySelectorAll(".form-subpage-select > *:not([data-subpage=" + subpage + "]).active"))
				e.classList.remove(CSS_ACTIVE);
		});
}

function form_row_build() {
	return {
		row: document.createElement("div"),
		append: function (element) {
			if (element)
				this.row.appendChild(element);
			return this;
		},
		append_text: function (text, classes) {
			return this.append(form_text_create(text, classes));
		},
		on_click: function (listener) {
			if (listener)
				this.row.addEventListener(CLICK, listener);
			return this;
		},
		on_delete: function (listener) {
			if (listener)
				this.row.appendChild(form_row_remove_create(this.row, listener));
			return this;
		},
		finish: function (parent, before) {
			if (!before)
				parent.appendChild(this.row);
			else
				parent.insertBefore(this.row, before);
			this.row = undefined;
		},
	}
}

function form_row_remove_create(row, func) {
	let div = document.createElement("div");
	form_row_remove_init(div, row, func);
	return div;
}

function form_row_remove_init(btn_remove, row, func) {
	btn_remove.textContent = "\u274C";
	btn_remove.addEventListener(CLICK, e => {
		let remove = true;
		if (func)
			remove = func.call(e.target, e);

		//if (remove)
			row.remove();
		e.stopPropagation();
	});
	btn_remove.addEventListener("mouseenter", function () { this.parentElement.classList.add(CSS_DELETE_HOVER) });
	btn_remove.addEventListener("mouseleave", function () { this.parentElement.classList.remove(CSS_DELETE_HOVER) });
	btn_remove.classList.add(CSS_BTN_REMOVE);
}

function form_field_convert_percent(element) {
	let v = element.textContent;
	v = Number(v.indexOf("%") === -1 ? v : v.substring(0, v.indexOf("%")));
	element.textContent = v + "%";
	return v / 100.0;
}

function grades_chart_init(chart, datasets) {
	new Chart(chart.getContext('2d'), {
		type: "line",
		data: { datasets: datasets },
		options: {
			animation: false,
			responsive: true,
			scales: {
				xAxes: [{ type: "time", time: { unit: "day" } }],
				yAxes: [{
					ticks: {
						reverse: true,
						min: 0.5,
						max: 5.5,
						suggestedMin: 0.5,
						suggestedMax: 5.5
					}
				}]
			}
		}
	});
}

async function update_push(method, path, msg_success, msg_error, body) {
	let response = await fetch(path, { method: method, body: body ? JSON.stringify(body) : undefined });

	if (response.status < 200 && response.status >= 300) {
		if (msg_error) toast_msg_show("error", msg_error + ": " + await response.text());
		return;
	}

	if (msg_success)
		toast_msg_show("success", msg_success);

	if (response.status !== 204)
		return response.json();
}

function set_template(root, template) {
	root.innerHTML = "";
	root.appendChild(document.importNode(template.content, true));
}

async function push_state(url) {
	window.history.pushState({}, "", url);
	await on_popstate();
}

function download_file(filename, content) {
	let a = document.createElement("a");
	a.setAttribute("href", content);
	a.setAttribute("download", filename);
	a.style.display = "none";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
}

function upload_file(on_change) {
	let input = document.createElement("input");
	input.type = "file";
	input.style.display = "none";
	input.addEventListener("change", on_change);
	document.body.appendChild(input);
	input.click();
	document.body.removeChild(input);
}

function toast_msg_show(kind, msg) {
	let message = document.createElement("div");
	message.textContent = msg;

	let bar = document.createElement("div");
	bar.classList.add(CSS_TOAST_MSG_BAR);

	let container = document.createElement("div");
	container.classList.add(CSS_TOAST_MSG, "msg-" + kind);
	container.appendChild(message);
	container.appendChild(bar);
	container.addEventListener(CLICK, () => container.remove());
	toast_msg_container.appendChild(container);
	window.setTimeout(node => node.remove(), 5000, container);
}

/// displays the given error message in the content area
function content_msg_show(msg) {
	let content_ = document.importNode(tpl_error_msg.content, true);
	content.innerHTML = "";
	content.append(content_);
	document.getElementById("msg").textContent = msg;
	btn_retry.addEventListener(CLICK, () => window.dispatchEvent(new Event("popstate")));
}

function create_grade_input(value, category, listener) {
	let grade_value_container = document.createElement("div");
	grade_value_container.style.gridAutoFlow = "column";
	grade_value_container.style.gridAutoColumns = "1fr 1em";
	grade_value_container.appendChild(form_input_text_create(value, listener));
	let div = form_text_create(category ? CATEGORY_INPUT_CHAR[category.input] : "?");
	div.style.placeSelf = "center";
	grade_value_container.appendChild(div);
	return grade_value_container;
}

function create_competences_list(i, subject, grade, parent) {
	let status = document.createElement("div");

	if ((grade.final_comp_passed & (1 << i)) !== 0) {
		status.classList.add(CSS_COMPETENCE_LIST_PASSED);
		status.textContent = "Passed";
	} else if ((grade.final_comp_failed & (1 << i)) !== 0) {
		status.classList.add(CSS_COMPETENCE_LIST_FAILED);
		status.textContent = "Failed";
	} else {
		status.classList.add(CSS_COMPETENCE_LIST_NONE);
	}

	form_row_build()
		.append_text(i + 1, [CSS_LIST_LINE_NUMBER])
		.append_text(subject.competences[i])
		.append(status)
		.finish(parent);
}

function create_competences_input(grade, subject) {
	let competences = document.createElement("div");
	competences.classList.add("grade-competence-container")

	for (let i = 0; i < subject.competences.length; i++) {
		let input = document.createElement("div");
		if (grade)
			input.classList.add("grade-competence-checkbox", (grade.comp_passed & (1 << i)) !== 0 ? "grade-competence-passed"
				: (grade.comp_failed & (1 << i)) !== 0 ? "grade-competence-failed"
				: "grade-competence-none");
		else
			input.classList.add("grade-competence-checkbox", "grade-competence-none");

		input.addEventListener(CLICK, () => {
			if (input.classList.contains("grade-competence-passed")) {
				input.classList.remove("grade-competence-passed");
				input.classList.add("grade-competence-failed");
				grade.comp_passed &= !(1 << i);
				grade.comp_failed |= 1 << i;
			} else if (input.classList.contains("grade-competence-failed")) {
				input.classList.remove("grade-competence-failed");
				input.classList.add("grade-competence-none");
				grade.comp_failed &= !(1 << i);
			} else if (input.classList.contains("grade-competence-none")) {
				input.classList.remove("grade-competence-none");
				input.classList.add("grade-competence-passed");
				grade.comp_passed |= 1 << i;
			}
		})
		competences.append(input);
	}

	return competences;
}

function lookup(array, id) {
	return array.find(v => v.id === id);
}

function chart_color(...inputs) {
	let s = "", hash = 0, i, chr;

	for (const input of inputs)
		s += input;

	for (i = 0; i < s.length; i++) {
		chr   = s.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // convert to 32bit integer
	}

	return "rgb(" + (hash & 0xFF) + "," + (hash >> 8 & 0xFF) + "," + (hash >> 16 & 0xFF) + ")"
}

function date_form_reql(date) {
	return new Date(date.epoch_time * 1000)
}

function date_to_input_value(date) {
	return date.getFullYear()
		+ "-" + (date.getMonth() + 1).toString().padStart(2, "0")
		+ "-" + date.getDate().toString().padStart(2, "0");
}

/// Converts a grade/points/percent to a normalized grade
function grade_from_input(val, category) {
	let grade, factor, ranges = [...category.ranges, 0.0];

	if (typeof val !== "string")
		throw "Invalid Grade value: " + val;

	if (category.input === CATEGORY_INPUT_MARK) {
		if (val.startsWith("+")) {
			grade = parseInt(val.slice(1));
			factor = 0.75;
		} else if (val.startsWith("-")) {
			grade = parseInt(val.slice(1));
			factor = 0.25;
		} else {
			grade = parseInt(val);
			factor = 0.50;
		}

		if (isNaN(grade) || grade < 1 || grade > 5)
			throw "Invalid grade value: " + val;

		return GRADE_RANGES_NORM[grade] + (GRADE_RANGES_NORM[grade - 1] - GRADE_RANGES_NORM[grade]) * factor;
	} else if (category.input === CATEGORY_INPUT_SYMBOL) {
		let grade = GRADE_SYMBOLS.indexOf(val);

		if (grade === -1)
			throw "Invalid grade value: " + val;

		return GRADE_RANGES_NORM[grade] + (GRADE_RANGES_NORM[grade - 1] - GRADE_RANGES_NORM[grade]) * 0.5;
	} else if (category.input === CATEGORY_INPUT_POINTS) {
		val = parseInt(val);
		grade = ranges.findIndex(v => val > v);

		if (grade === -1)
			throw "Invalid grade value: " + val;

		if (category.calc === CATEGORY_CALC_LINEAR)
			factor = (val - ranges[grade]) / (ranges[grade - 1] - ranges[grade])
		else
			factor = 0.5;

		return GRADE_RANGES_NORM[grade] + (GRADE_RANGES_NORM[grade - 1] - GRADE_RANGES_NORM[grade]) * factor;
	} else
		throw "Invalid category input value: " + category.input;
}

function grade_calc_final(grades, schug82e) {
	let total_weight = 0, final_grade = 0, category_counts = new Map();
	let comp_passed = 0, comp_failed = 0;

	grades
		.sort((a, b) => (a.date.epoch_time < b.date.epoch_time) ? -1 : (a.date.epoch_time > b.date.epoch_time) ? 1 : 0)
		.forEach(v => {
			if (!category_counts.has(v.category))
				category_counts.set(v.category, 0);
			category_counts.set(v.category, category_counts.get(v.category) + 1);
		});

	for (const grade_data of grades) {
		let category = lookup(data.categories, grade_data.category);

		if (!category)
			throw "Invalid category: " + grade_data.category;

		let grade = grade_from_input(grade_data.value, category);
		let weight = Math.min(category.weight, category.weight_max / category_counts.get(grade_data.category));

		final_grade += grade * weight;
		total_weight += weight;
		grade_data.grade = grade;
		grade_data.final = final_grade / total_weight;

		if (schug82e) {
			comp_passed |= grade_data.comp_passed;
			comp_failed |= grade_data.comp_failed;
			comp_failed &= ~comp_passed;
			grade_data.final_comp_passed = comp_passed;
			grade_data.final_comp_failed = comp_failed;
		}
	}

	if (comp_failed !== 0)
		grades[grades.length - 1].final = Math.min(0.4999, grades[grades.length - 1].final);
}

function grade_to_string(grade, category) {
	let idx = GRADE_RANGES_NORM.findIndex(v => grade > v);

	if (category.output === CATEGORY_OUTPUT_MARK)
		return GRADE_VALUES[idx - 1].toString();
	else if (category.output === CATEGORY_OUTPUT_SYMBOL)
		return GRADE_SYMBOLS[idx - 1];
	else
		return "-";
}

function grade_to_number(grade) {
	let idx = GRADE_RANGES_NORM.findIndex(v => grade > v);
	return GRADE_RANGES[idx] - (GRADE_RANGES[idx] - GRADE_RANGES[idx - 1])
		* ((grade - GRADE_RANGES_NORM[idx]) / (GRADE_RANGES_NORM[idx - 1] - GRADE_RANGES_NORM[idx]));
}

function seq_diff(original, updated) {
	let updates = undefined;

	for (const e of original)
		if (!updated || !updated.includes(e)) {
			if (!updates)
				updates = {};
			if (!updates.del)
				updates.del = [];
			updates.del.push(e);
		}

	for (const e of updated)
		if (!original || !original.includes(e)) {
			if (!updates)
				updates = {};
			if (!updates.add)
				updates.add = [];
			updates.add.push(e);
		}

	return updates;
}

Array.prototype.groupBy = function (key, accum, init) {
	let map = new Map();

	for (const e of this) {
		let k = key(e);

		if (init) {
			if (!map.has(k))
				map.set(k, init());

			accum(map.get(k), e);
		} else {
			if (!map.has(k))
				map.set(k, e);
			else
				accum(map.get(k), e);
		}
	}

	return [...map.values()];
}

Array.prototype.remove_eq = function (v) {
	let idx = this.indexOf(v);

	if (idx !== -1)
		this.splice(idx);
}